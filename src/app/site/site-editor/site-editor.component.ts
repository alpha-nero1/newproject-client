/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { SitesService } from 'src/app/site/sites.service';
import { COMMA, ENTER, BACKSPACE } from '@angular/cdk/keycodes';
import { Site } from '../site.model';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Utils } from '../../utils/utils';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatChipInputEvent } from '@angular/material/chips';
import { DialogService } from '../../dialog/dialog.service';
import { ImagePickerComponent } from '../../shared/image-picker/image-picker/image-picker.component';
import { AuthService } from '../../auth/auth.service';

/**
 * @author Alessandro Alberga
 * @description Re-usable component for creating and editing sites.
 */
@Component({
  selector: 'app-site-editor',
  templateUrl: './site-editor.component.html',
  styleUrls: ['./site-editor.component.less'],
})
export class SiteEditorComponent implements OnInit, OnDestroy {

  /**
   * Site object to use to toggle if editing
   * instead of creating
   */
  public site = new Site({
    title: '',
    tags: [],
    content: ''
  });

  /**
   * Simple boolean flag to see if we are editing or creating sites.
   */
  public isEditing = false;

  /**
   * Let's us indicate to the user if the site is loading or not.
   */
  public isLoading = false;

  /**
   * Key codes for mat chips to recognise.
   */
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, BACKSPACE];

  /**
   * Router url subscription reference.
   */
  private _routerSubscription: Subscription;

  /**
   * Stage a banner photo to be uploaded on submit.
   */
  private stagedBannerFileToUpload: any;

  /**
   * The staged url we display to the user.
   */
  private stagedBannerFileSourceUrl: any;

  /**
   * Constructor.
   */
  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _sitesService: SitesService,
    private _snack: MatSnackBar,
    private dialogService: DialogService,
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.subscribeToActiveRoute();
  }

  /**
   * Subscribe to activated route to determine if editing or not.
   */
  private subscribeToActiveRoute(): void {
    this._routerSubscription = this._activatedRoute.params.subscribe(params => {
      if (params.edit) {
        this.isEditing = true;
        this.isLoading = true;
        if (params.uuid) {
          // Fetch the site we want to edit.
          this._sitesService.getSiteByUuid(params.uuid)
            .then((site: Site) => {
              this.site = site;
              this.isLoading = false;
            })
            .catch(err => {
              this.isLoading = false;
              throw err;
            });
        }
      } else {
        this.isEditing = false;
      }
    });
  }

  /**
   * Add method for mat tags.
   */
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add tag
    if ((value || '').trim()) {
      this.site.tags.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  /**
   * Remove tags from mat tags.
   */
  remove(tag: string): void {
    const index = this.site.tags.indexOf(tag);

    if (index >= 0) {
      this.site.tags.splice(index, 1);
    }
  }

  /**
   * get only the values we want to mutate. Any others will just
   * cause sequelize to throw an error.
   */
  private _transformToNewValues(): any {
    return Utils.GetObjectWithSpecificProps(this.site, ['title', 'content', 'tags', 'banner_photo']);
  }

  /**
   * On creating a site.
   * @param form the ngForm to evaluate.
   */
  public onSubmit(form: any): any {
    if (!form.valid || !this.site.content) { return; }
    return new Promise((resolve) => {
      if (this.stagedBannerFileToUpload) {
        return this.authService.uploadFile(this.stagedBannerFileToUpload)
        .then(url => resolve(url));
      }
      resolve(null);
    })
    .then((bannerUrl: string) => {
      this.site.banner_photo = bannerUrl;
      if (this.isEditing) {
        return this._sitesService.updateSite(this.site.id, this._transformToNewValues())
          .then(this.saveHandler);
      } else {
        delete this.site.uuid;
        return this._sitesService.createSite(this.site)
          .then(this.saveHandler);
      }
    })
  }

  /**
   * Cancel the edit, just go ack to the view page.
   */
  public cancelEdit(): void {
    this._router.navigate([`/sites/${this.site.uuid}`]);
  }

  /**
   * On success, navigate to the site created/updated.
   */
  private saveHandler = data => {
    if (data && data.uuid ) {
      this._router.navigate([`/sites/${data.uuid}`]);
      if (this.isEditing) {
        this._snack.open('Site update successfull', 'Ok', { duration: 3000 });
      } else {
        this._snack.open('Site create successfull', 'Ok', { duration: 3000 });
      }
    }
  }

  /**
   * Banner clicked handler.
   */
  public bannerClickedHandler(): any {
    // Back out if someone else tries to upload.
    this.dialogService.openFormDialog(
      {
        componentInputs: {
          imageSource: this.site.banner_photo,
          imageDisplayDimensions: {
            width: '70vw',
            height: '30vh'
          }
        },
        formInputs: {
          component: ImagePickerComponent,
          submitText: 'Save',
          title: 'Choose a site banner'
        }
      },
      (file: File) => {
       if (file) {
        this.stagedBannerFileToUpload = file;
        const fileReader = new FileReader();
        fileReader.onload = e => this.stagedBannerFileSourceUrl = fileReader.result;
        fileReader.readAsDataURL(file);
      }
    });
  }

  /**
   * Clear any banner photo stuffs...
   */
  public clearBannerPhoto(ev): void {
    ev.stopPropagation();
    this.stagedBannerFileToUpload = null;
    this.stagedBannerFileSourceUrl = null;
    this.site.banner_photo = null;
  }

  ngOnDestroy(): void {
    if (this._routerSubscription) {
      this._routerSubscription.unsubscribe();
    }
  }
}
