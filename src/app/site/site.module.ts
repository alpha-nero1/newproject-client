/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteComponent } from './site/site.component';
import { SiteEditorComponent } from './site-editor/site-editor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SitesService } from './sites.service';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';

import { SiteRoutingModule } from './site-routing.module';
import { NavbarModule } from '../navbar/navbar.module';
import { FooterModule } from '../footer/footer.module';
import { MainModuleComponentModule } from '../main-module-component/main-module-component.module';
import { VoteModule } from '../shared/vote/vote.module';
import { UserAvatarModule } from '../shared/user-avatar/user-avatar.module';
import { NpEditorComponent } from './np-editor/np-editor.component';
import { AdModule } from '../shared/ad/ad.module';
import { DialogModule } from '../dialog/dialog.module';
import { ReportSiteComponent } from './report-site/report-site.component';
import { IconButtonModule } from '../shared/icon-button/icon-button.module';
import { MatRadioModule } from '@angular/material/radio';
import { CommentModule } from '../comment/comment.module';

/**
 * @author Alessandro Alberga
 * @description Site module.
 */
@NgModule({
  declarations: [
    SiteComponent,
    SiteEditorComponent,
    NpEditorComponent,
    ReportSiteComponent
  ],
  imports: [
    AdModule,
    CKEditorModule,
    CommonModule,
    CommentModule,
    DialogModule,
    FlexLayoutModule,
    FooterModule,
    FormsModule,
    MainModuleComponentModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    IconButtonModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatTooltipModule,
    NavbarModule,
    ReactiveFormsModule,
    SiteRoutingModule,
    UserAvatarModule,
    VoteModule
  ],
  providers: [SitesService]
})
export class SiteModule { }
