import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SiteEditorComponent } from './site-editor/site-editor.component';
import { SiteComponent } from './site/site.component';
import { MainModuleComponent } from '../main-module-component/main-module/main-module.component';

const routes: Routes = [
  {
    path: '',
    component: MainModuleComponent,
    children: [
      { path: 'create-site', component: SiteEditorComponent },
      { path: ':uuid/:edit', component: SiteEditorComponent },
      { path: ':uuid', component: SiteComponent }
    ]
  }
];

/**
 * @author Alessandro Alberga
 * @descritpion routing for the site module.
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteRoutingModule {}
