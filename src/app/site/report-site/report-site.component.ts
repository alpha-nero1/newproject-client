/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

/**
 * @author Alessandro Alberga
 * @description A light weight form that allows users to report a site.
 */
@Component({
  selector: 'app-report-site',
  templateUrl: './report-site.component.html',
  styleUrls: ['./report-site.component.less']
})
export class ReportSiteComponent {

  /**
   * Report site form used in template.
   */
  public reportSiteForm = new FormGroup({
    reason: new FormControl('', [Validators.required])
  });

  constructor() { }

  /**
   * Submit handle.
   */
  public submit(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.reportSiteForm.valid) {
        resolve(this.reportSiteForm.value);
      } else {
        reject('Form invalid.');
      }
    });
  }
}
