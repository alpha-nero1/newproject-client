/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSiteComponent } from './report-site.component';
import { AdModule } from '../../shared/ad/ad.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { CommonModule } from '@angular/common';
import { CommentModule } from '../../comment/comment.module';
import { DialogModule } from '../../dialog/dialog.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FooterModule } from '../../footer/footer.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainModuleComponentModule } from '../../main-module-component/main-module-component.module';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { IconButtonModule } from '../../shared/icon-button/icon-button.module';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NavbarModule } from '../../navbar/navbar.module';
import { SiteRoutingModule } from '../site-routing.module';
import { UserAvatarModule } from '../../shared/user-avatar/user-avatar.module';
import { VoteModule } from '../../shared/vote/vote.module';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ReportSiteComponent', () => {
  let component: ReportSiteComponent;
  let fixture: ComponentFixture<ReportSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSiteComponent ],
      imports: [
        AdModule,
        BrowserAnimationsModule,
        CKEditorModule,
        CommonModule,
        CommentModule,
        DialogModule,
        FlexLayoutModule,
        FooterModule,
        FormsModule,
        MainModuleComponentModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatChipsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatRadioModule,
        IconButtonModule,
        MatMenuModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatTooltipModule,
        NavbarModule,
        ReactiveFormsModule,
        RouterTestingModule,
        SiteRoutingModule,
        UserAvatarModule,
        VoteModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
