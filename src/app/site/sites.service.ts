/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Injectable } from '@angular/core';
import { GraphQLService } from '../utils/graphQL.service';
import { Site, SiteGqlType } from './site.model';
import PromiseProvider from '../utils/promiseProvider';
import { Comment, CommentGql } from '../comment/types/comment.model';
import { SiteReport, SiteReportGql } from '../admin/types/site-report.interface';
import { DailyFeatureGql, DailyFeature } from '../admin/types/daily-feature.interface';
import { GeneralQueryOpts } from '../shared/types/filter.interface';
import { RowsAndCount } from '../common/types/rows-and-count.interface';
import { VoteGql } from '../shared/vote/types/vote.interface';

/**
 * @author Alessandro Alberga
 * @description Angular service for all sites functionality.
 */
@Injectable({
  providedIn: 'root'
})
export class SitesService extends PromiseProvider {
  constructor(private gqlService: GraphQLService) {
    super();
  }

  /**
   * Returns created site.
   *
   * @param site site object.
   */
  public createSite(site: Site): Promise<Site> {
    const mutation = `
      mutation ($site: SiteInput!) {
        createSite(site: $site) {
          uuid
        }
      }`;
    return this.queryToPromise(this.gqlService.runMutation(mutation, { site }))
      .then(res => res.createSite);
  }

  /**
   * Update a site.
   *
   * @param id id of site to update.
   * @param newValues new values of the site.
   */
  public updateSite(id: number, newValues: Site) {
    const vars = { id, new_values: newValues };
    const mutation = `
      mutation ($id: Int!, $new_values: SiteInput!) {
        updateSite(id: $id, new_values: $new_values) {
          uuid
        }
      }
    `;
    return this.queryToPromise(this.gqlService.runMutation(mutation, vars))
      .then(res => res.updateSite);
  }

  /**
   * Get the site contents from its id.
   *
   * @param id id of the site.
   */
  public getSiteByUuid(uuid: string): Promise<Site> {
    const query = ` {
      getSiteByUuid(uuid: "${uuid}") {
        ${SiteGqlType()}
        reports {
          id
          reporting_user_id
        }
        comments {
          count
          rows {
            ${CommentGql}
          }
        }
      }
    }`;
    return this.queryToPromise(this.gqlService.runQuery(query))
      .then(res => new Site(res.getSiteByUuid));
  }

  /**
   * Reach the Api to get the sites that are currently trending.
   *
   * @param opts general query opts.
   */
  public async getTrendingSites(opts: GeneralQueryOpts): Promise<RowsAndCount<Site>> {
    const query = `query($query_opts: GeneralQueryOpts) {
      listTrendingSites(query_opts: $query_opts) {
        count
        rows {
          ${SiteGqlType()}
        }
      }
    }`;
    const res = await this.queryToPromise(this.gqlService.runQuery(query, {
      query_opts: opts
    }));
    return res.listTrendingSites;
  }

  /**
   * Reach api to get reccommended sites for a user.
   *
   * @param opts general query opts.
   */
  public async getRecommendedSites(opts: GeneralQueryOpts): Promise<RowsAndCount<Site>>  {
    const query = `query($query_opts: GeneralQueryOpts) {
      listRecommendedSitesByUser(query_opts: $query_opts) {
        count
        rows {
          ${SiteGqlType()}
        }
      }
    }`;
    const res = await this.queryToPromise(this.gqlService.runQuery(query, {
      query_opts: opts
    }));
    return res.listRecommendedSitesByUser;
  }

  /**
   * Comment on a site.
   *
   * @param siteId id of site to comment on.
   * @param comment comment object.
   */
  public async commentOnSite(siteId: number, comment: Comment): Promise<Comment[]> {
    const mutation = `mutation ($site_id: Int!, $comment: CommentInput!) {
      commentOnSite(comment: $comment, site_id: $site_id) {
        ${CommentGql}
      }
    }`;
    const res = await this.queryToPromise(this.gqlService.runMutation(mutation, {
      comment,
      site_id: siteId
    }));
    return res.commentOnSite;
  }

  /**
   * Search sites in the database according to parameters.
   *
   * @param keyword search keyword.
   * @param tags tags to filter by.
   * @param opts general query opts.
   */
  public async searchSites(keyword: string, tags: string[], opts: GeneralQueryOpts): Promise<RowsAndCount<Site>> {
    const query = `query($keyword: String, $tags: [String], $query_opts: GeneralQueryOpts) {
      searchSites(keyword: $keyword, tags: $tags, query_opts: $query_opts) {
        count
        rows {
          ${SiteGqlType()}
        }
      }
    }`;
    const res = await this.queryToPromise(this.gqlService.runQuery(query, {
      keyword,
      tags,
      query_opts: opts
    }));
    return res.searchSites;
  }

  /**
   * Get comments of a particular site.
   *
   * @param siteId id of site to query for comments.
   * @param queryOpts general query options.
   */
  public getSiteComments(siteId: number, queryOpts: GeneralQueryOpts): Promise<RowsAndCount<Comment>> {
    const query = `query ($site_id: Int!, $query_opts: GeneralQueryOpts) {
      findCommentsBySite(site_id: $site_id, query_opts: $query_opts) {
        count
        rows {
          ${CommentGql}
        }
      }
    }`;
    return this.queryToPromise(this.gqlService.runQuery(query, {
      site_id: siteId,
      query_opts: queryOpts
    }))
    .then(res => res.findCommentsBySite);
  }

  /**
   * Vote on a site. return voting results.
   *
   * @param siteId id of site to vote on.
   * @param up wether up or downvote is happening.
   */
  public async voteOnSite(siteId: number, up: boolean): Promise<any> {
    const mutation = `mutation {
      voteOnSite(site_id: ${siteId}, up: ${up}) {
       ${VoteGql}
      }
    }`;
    const res = await this.queryToPromise(this.gqlService.runMutation(mutation, {}));
    return res.voteOnSite;
  }

  /**
   * Report a site.
   *
   * @param siteId id of the site.
   * @param reason reason of the report.
   */
  public reportSite(siteId: number, reason: string): Promise<SiteReport> {
    const mutation = `mutation($site_id: Int!, $reason: String) {
      reportSite(site_id: $site_id, reason: $reason) {
        ${SiteReportGql}
      }
    }`;
    return this.queryToPromise(this.gqlService.runMutation(mutation, {
      site_id: siteId,
      reason
    }))
    .then(res => res.reportSite);
  }

  /**
   * Dismiss reports.
   *
   * @param siteReportIds ids to dismiss.
   */
  public getDailyFeatures(opts: GeneralQueryOpts): Promise<{ count: number, rows: DailyFeature[] }> {
    const endpoint = 'getDailyFeatures';
    const gql = `
      query ($opts: GeneralQueryOpts!) {
        ${endpoint}(opts: $opts) {
          count
          rows {
            ${DailyFeatureGql}
          }
        }
      }
    `;
    return this.queryToPromise(this.gqlService.runQuery(gql, { opts }))
    .then(res => res[endpoint]);
  }
}
