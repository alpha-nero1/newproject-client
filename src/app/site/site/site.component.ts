/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SitesService } from 'src/app/site/sites.service';
import { Subscription, BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Site } from '../site.model';
import { Comment } from '../../comment/types/comment.model';
import { AccountService } from 'src/app/account/account.service';
import { Utils } from 'src/app/utils/utils';
import { UserAware } from '../../common/user-aware.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DialogService } from '../../dialog/dialog.service';
import { ReportSiteComponent } from '../report-site/report-site.component';
import { PagedDatastoreDelegate } from '../../common/paged-datastore-delegate/paged-datastore-delegate';

/**
 * @author Alessandro Alberga
 * @description Site component.
 */
@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.less'],
  // Fix for innerHtml styling.
  encapsulation: ViewEncapsulation.None,
})
export class SiteComponent extends UserAware implements OnInit, OnDestroy {
  /**
   * Site user is currenlty viewing.
   */
  public site: Site = new Site({});

  /**
   * Toggle variable tracking if the user is subscribed to the sites user or not.
   */
  public subscribed = false;

  /**
   * Subscription to current url parameters.
   */
  private paramSubscription: Subscription;

  /**
   * The uuid of the site.
   */
  private uuid = '';

  /**
   * Local reference of date function from utils to use in template.
   */
  public transformToLocaleDate = Utils.TransformToLocaleDate;

  /**
   * Flag if the site has been reported or not.
   */
  public siteReported = false;

  /**
   * Comments paging delegate object.
   */
  public commentsPagingDelegate: PagedDatastoreDelegate<Comment>;

  constructor(
    private siteService: SitesService,
    private activeRoute: ActivatedRoute,
    authService: AuthService,
    private accountService: AccountService,
    private router: Router,
    private snack: MatSnackBar,
    private dialogService: DialogService
  ) {
    super(authService);
    this.initCallback((user) => {
      this.paramSubscription = this.activeRoute.params
        .subscribe(params => {
          this.uuid = params.uuid;
          this.loadSite();
        });
    });
  }

  ngOnInit(): void { }

  /**
   * Load a site from uuid
   *
   * @param uuid string uuid of the site.
   */
  async loadSite(): Promise<void> {
    this.site = await this.siteService.getSiteByUuid(this.uuid);
    this.commentsPagingDelegate = new PagedDatastoreDelegate<Comment>({
      addItemCallback: (comment) => this.siteService.commentOnSite(this.site.id, comment),
      loadResultsCallback: (opts) => this.siteService.getSiteComments(this.site.id, opts),
      preserveAttributes: ['replies']
    });
    this.site.content = `
      <div class="content-container">
        ${this.site.content}
      </div>
    `;
    this.site.vote_count = (this.site?.upvotes?.count - this.site?.downvotes?.count);
    // Transform into a readable date string.
    this.subscriptionCheck();
    // Check if the site was reported or not.
    this.siteReported = Boolean(this.site.reports && this.site.reports.find(report => report.reporting_user_id === this.user.id));
  }

  /**
   * Delete a comment using the sites service.
   * @param commentId used for the deletion.
   */
  public deleteComment(commentId: number) {
    return this.commentsPagingDelegate.removeItemFromDatastore(commentId);
  }

  /**
   * Move to the editor.
   */
  public toEdit() {
    // NOTE: additional checks please.
    this.router.navigate([`/sites/${this.site.uuid}/edit`]);
  }

  /**
   * Do a check if the viewing users id is part of the site users
   * subscribed list, toggle the subscribed variable appropriately.
   */
  private subscriptionCheck(): void {
    const subscibed = (userId, subscriptionRows) => (
      Utils.AmISubscribed(
        userId,
        subscriptionRows
      )
    );
    this.subscribed = subscibed(this.site.user.id, this.user.subscriptions.rows);
  }

  /**
   * Function that handles commenting.
   */
  public async onComment(comment: string) {
    const commentToSave: Comment = { text: comment };
    // Create new comment and store.
    await this.siteService.commentOnSite(this.site.id, commentToSave);
    return this.siteService.getSiteComments(this.site.id, this.commentsPagingDelegate.queryOpts)
    .then(({ rows, count }) => {
      this.commentsPagingDelegate.upsertDataStore(
        this.commentsPagingDelegate.queryOpts.page,
        rows
      );
    });
  }

  /**
   * Vote on a site.
   *
   * @param up up or downvote.
   */
  public async onVote(up: boolean): Promise<void> {
    // Execute the vote.
    const response = await this.siteService.voteOnSite(this.site.id, up);
    // Refresh up and downvotes.

    // Load in the votes and refresh.
    this.site = {
      ...this.site,
      vote_count: (response?.upvotes?.count - response?.downvotes?.count)
    };
  }

  /**
   * Subscribe to the user who made the site.
   */
  public onSubscribe(subscribe: boolean): void {
    const subscribeeId = this.site.user.id;
    if (subscribe) {
      this.accountService.subscribe(subscribeeId).then(() => {
        this.snack.open(
          `Subscribed to ${this.site.user.username}`,
          'Ok',
          { duration: 4000 }
        );
      });
    } else {
      this.accountService.unsubscribe(subscribeeId).then(() => {
        this.snack.open(
          `Unsubscribed to ${this.site.user.username}`,
          'Ok',
          { duration: 4000 }
        );
      });
    }
    this.subscriptionCheck();
  }

  /**
   * Open the report dialog component.
   */
  public openReportDialog(): void {
    if (this.siteReported) {
      // Short circuit if already reported and do not go ahead.
      return;
    }
    this.dialogService.openFormDialog({
      formInputs: {
        component: ReportSiteComponent,
        submitText: 'Report',
        title: 'Report this site?'
      }
    }, (res) => {
      if (res && res.reason) {
        return this.siteService.reportSite(this.site.id, res.reason)
        .then(siteReport => {
          this.siteReported = Boolean(siteReport && siteReport.id);
        });
      }
    });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
