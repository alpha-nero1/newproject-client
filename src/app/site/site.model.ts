/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Comment } from '../comment/types/comment.model';
import { VoteResponse } from '../common/voteResponse.model';
import { User } from '../auth/user.model';
import { SiteReport } from '../admin/types/site-report.interface';
import { VariableInstanceLoader } from '../common/graphql-helpers/variable-instance-loader';
import { RowsAndCount } from '../common/types/rows-and-count.interface';
import { VoteGql, Vote } from '../shared/vote/types/vote.interface';

/**
 * @author Alessandro Alberga
 * @class Site
 * @description Site model class.
 */
export class Site {
  /**
   * Site id.
   */
  id?: number;

  /**
   * Uuid of site.
   */
  uuid?: string;

  /**
   * Site title.
   */
  title?: string;

  /**
   * Html content of site.
   */
  content?: string;

  /**
   * User id who made site.
   */
  user_id?: number;

  /**
   * Included user model.
   */
  user: User;

  /**
   * Tags asociated with site.
   */
  tags: string[];

  /**
   * Date site was made.
   */
  created_at?: string | Date;

  /**
   * Last update.
   */
  updated_at?: Date;

  /**
   * Response of votes from api.
   */
  votes: VoteResponse;

  /**
   * Consolidated number of votes for a site.
   */
  vote_count?: number;

  /**
   * Comments on the site.
   */
  comments?: RowsAndCount<Comment>;

  /**
   * Upvote res.
   */
  upvotes?: RowsAndCount<Vote>;

  /**
   * Downvote res.
   */
  downvotes?: RowsAndCount<Vote>;

  /**
   * Site reports that could exist on a site.
   */
  reports?: SiteReport[];

  /**
   * Site's banner photo.
   */
  banner_photo: any;

  constructor(siteDataObject: any) {
    VariableInstanceLoader(this, siteDataObject);
  }
}

/**
 * Mimics the backend gql string representing a site.
 * Allows us to use the type here in the sense of getting
 * the right return values.
 */
export const SiteGqlType = (aditionalFields = '') => `
  id
  uuid
  title
  content
  user {
    id
    username
    first_name
    last_name
    email
    profile_photo
  }
  tags
  banner_photo
  created_at
  updated_at
  ${VoteGql}
  ${aditionalFields}
`;

/**
 * Sites with count interface.
 */
export interface SitesWithCount {
  count: number;
  rows: Site[];
}
