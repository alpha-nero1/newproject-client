/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { AwsS3Service } from '../../third-party/aws.s3.service';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { NPUploadAdapter } from '../../third-party/CKEditor/np-upload-adapter';
import { CKEditorComponent } from '@ckeditor/ckeditor5-angular';

/**
 * @author Alessandro Alberga
 * @description custom ckeditor implementation.
 */
@Component({
  selector: 'app-np-editor',
  templateUrl: './np-editor.component.html',
  styleUrls: ['./np-editor.component.css']
})
export class NpEditorComponent implements OnInit {

  /**
   * Content string input.
   */
  @Input() content = '';

  /**
   * On content changed event emitter.
   */
  @Output() contentChange = new EventEmitter<string>();

  /**
   * Reference to the classic editor class.
   */
  public Editor = ClassicEditor;

  /**
   * Editor config object we need to configure our custom uploader
   * plugin.
   */
  public editorConfig: any = {};

  constructor(private awsS3Service: AwsS3Service) { }

  ngOnInit(): void {
    this.editorConfig.extraPlugins = [
      // This `stupid as ass` plugin is stopping us from es6 build target, keep checking back
      // here temporarily to see if can upgrade:
      // https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/frameworks/angular.html
      (editor) => new NPUploadAdapter(editor, this.awsS3Service)
    ];
  }

  /**
   * Function to execute on editor content change.
   *
   * @param event string value of the editor on change.
   */
  public contentWasChanged(event: string): void {
    this.content = event;
    this.contentChange.emit(this.content);
  }
}
