/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { S3 } from 'aws-sdk';
import { v4 as uuid } from 'uuid';
import { Injectable } from '@angular/core';
import { config } from '../config/config';
import PromiseProvider from '../utils/promiseProvider';
import { GraphQLService } from '../utils/graphQL.service';
import { STSTemporaryCredentials } from './STSTemporaryCredentials.interface';

@Injectable()
export class AwsS3Service extends PromiseProvider {

  constructor(private gqlService: GraphQLService) {
    super();
  }

  /**
   * Ask aws STS for those precious temporary creds.
   */
  private getTemporaryCredentials(): Promise<STSTemporaryCredentials> {
    const gql = `{
      getTemporaryCredentials {
        AccessKeyId
        SecretAccessKey
        SessionToken
      }
    }`;
    return this.queryToPromise(this.gqlService.runQuery(gql))
      .then(res => res.getTemporaryCredentials);

  }

  /**
   * Upload the user photo to AWS and return the response.
   */
  public async uploadUserPhoto(file: File): Promise<any> {
    const fileNameDeconstructed = file.name.split('.');
    const fileExtension = fileNameDeconstructed[fileNameDeconstructed.length - 1];
    const photoName = `${uuid()}.${fileExtension}`;
    const creds = await this.getTemporaryCredentials();
    const aws = new S3({
      accessKeyId: creds.AccessKeyId,
      secretAccessKey: creds.SecretAccessKey,
      sessionToken: creds.SessionToken,
      signatureVersion: 'v4',
      endpoint: `https://s3.ap-southeast-2.amazonaws.com`,
      region: config.awsS3.region
    });
    // Put the file up in s3 return response.
    return new Promise((resolve, reject) => {
      aws.putObject({
        Bucket: 'newproject25',
        ContentType: file.type,
        ACL: 'public-read',
        Body: file,
        Key: photoName
      }, (err, data) => {
        if (err) { reject(err); }
        // If we receive an err, throw it.
        if (data) {
          resolve(`${config.awsS3.url}/${photoName}`);
        }
      });
    });
  }
}
