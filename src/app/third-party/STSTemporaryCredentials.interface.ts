/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description interface defining what credentials we get back from the client
 *   looks like.
 */
export interface STSTemporaryCredentials {
  /**
   * Session token for configuration.
   */
  SessionToken: string;

  /**
   * Access key id for configuration.
   */
  AccessKeyId: string;

  /**
   * Secret access key for configuration.
   */
  SecretAccessKey: string;
}
