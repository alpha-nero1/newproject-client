/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

 // tslint:disable: variable-name

import { Subscription, BehaviorSubject } from 'rxjs';
import { AwsS3Service } from '../aws.s3.service';
import { config } from '../../config/config';

/**
 * @author Alessandro Alberga
 * @description custom asset upload adapter for the new project.
 */
export class NPUploadAdapter {

  private abortSubscription: Subscription;

  private abortExecutor = new BehaviorSubject<boolean>(false);

  private loader: any;

  constructor(editor, private awsS3Service: AwsS3Service) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader: any) => {
      this.loader = loader;
      return this;
    };
  }

  /**
   * Upload function CKEditor will execute instead of its own uploader.
   */
  public upload(): Promise<any> {
    return this.loader.file
      .then(file => this.sendRequest(file));
  }

  /**
   * Send to our custom file storage.
   *
   * @param file file that must be uploaded.
   */
  private sendRequest(file): Promise<any> {
    return new Promise((resolve, reject) => {
      // Listen for potential abort
      this.abortSubscription = this.abortExecutor.subscribe((abort) => {
        if (abort) { reject('**FATAL: Upload was aborted.'); }
      });
      // NOTE: must abort if file is too large.
      if (file.size > config.awsS3.uploadLimit) {
        reject('File is too large');
      }
      this.awsS3Service.uploadUserPhoto(file)
        .then((url: string) => resolve({ default: url }))
        .catch(() => { reject('**FATAL: Upload was unsuccessfull'); });
    });
  }

  /**
   * Send the abort flag to know to toggle the unsubscribe on reqests.
   */
  public abort(): void {
    // Send the abort flag.
    this.abortExecutor.next(true);
    // NOTE: this will execute on removal from the editor.
    // I'm assuming it will be smart to insert a delete request.
  }

  /**
   * Function ckeditor will execute on object destruction.
   */
  public destroy(): void {
    if (this.abortSubscription) {
      this.abortSubscription.unsubscribe();
    }
    if (this.abortExecutor) {
      this.abortExecutor.unsubscribe();
    }
  }
}
