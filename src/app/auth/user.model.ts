/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Site } from '../site/site.model';
import { SubscriptionResponse } from '../common/subscriptionResponse.model';
import { UserStats } from '../account/types/user-stats.interface';

/**
 * @author Alessandro Alberga
 * @description User class. All properties are optional to ensure
 *  that the update call runs smoothly.
 */
export class User {
  /**
   * Unique id of the user.
   */
  id?: number;

  /**
   * First name of user.
   */
  first_name = '';

  /**
   * Last name of user.
   */
  last_name = '';

  /**
   * Unique username of user.
   */
  username = '';

  /**
   * unique email of user.
   */
  email = '';

  /**
   * Avatar/image of user.
   */
  profile_photo = null;

  /**
   * Date user was created.
   */
  date?: Date;

  /**
   * DOB of user.
   */
  public date_of_birth?: Date;

  /**
   * Tags of user.
   */
  tags?: string[] = [];

  /**
   * Included sites model.
   */
  sites?: Site[] = [];

  /**
   * Included subscriptions model.
   */
  subscriptions?: SubscriptionResponse;

  /**
   * Included subscriptions (my subscribers) model.
   */
  subscribers?: SubscriptionResponse;

  /**
   * Role bits of user.
   */
  role_bits: number;

  /**
   * User statistics.
   */
  user_stats?: UserStats;

  /**
   * User's banner photo.
   */
  banner_photo: string;

  constructor(userDataObject: any = {}) {
    // Load in variables from an object. preserve the instance.
    if (userDataObject) {
      Object.keys(userDataObject).forEach(key => {
        this[key] = userDataObject[key];
      });
    }
  }

  /**
   * Shorthand for getting first and last name together.
   */
  get name() {
    return `${this.first_name} ${this.last_name}`;
  }
}
