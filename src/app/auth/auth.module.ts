/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GraphQLService } from '../utils/graphQL.service';
import { AuthRoutingModule } from './auth-routing.module';
import { PasswordResetModule } from '../shared/password-reset/password-reset.module';
import { ForgotPasswordModule } from '../shared/forgot-password/forgot-password.module';
import { DialogModule } from '../dialog/dialog.module';
import { AuthComponent } from './auth/auth.component';
import { FooterModule } from '../footer/footer.module';
import { LogoutComponent } from './logout/logout.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';

/**
 * @author Alessandro Alberga
 * @description Auth module.
 */
@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    AuthComponent,
    LogoutComponent
  ],
  imports: [
    AuthRoutingModule,
    CommonModule,
    DialogModule,
    FlexLayoutModule,
    FooterModule,
    ForgotPasswordModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatSnackBarModule,
    PasswordResetModule,
    ReactiveFormsModule
  ],
  providers: [
    GraphQLService,
    MatDatepickerModule
  ]
})
export class AuthModule {}
