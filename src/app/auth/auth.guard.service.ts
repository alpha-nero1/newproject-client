/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

/**
 * @author Alessandro Alberga
 * @description AuthGuardService to protect certain routes.
 */
@Injectable()
export class AuthGuardService implements CanActivate {
  /**
   * Constructor.
   *
   * @param authService service to check user is authenticated.
   * @param router router to redirect.
   */
  constructor(private authService: AuthService, private router: Router) {}

  /**
   * Checks if the user is logged in and re-directs if
   * necessary.
   */
  canActivate(): boolean {
    if (!this.authService.userIsAuthenticated()) {
      this.router.navigate(['/auth/login']);
      return false;
    }
    return true;
  }
}
