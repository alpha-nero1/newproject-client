/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { DialogService } from '../../dialog/dialog.service';
import { ForgotPasswordComponent } from '../../shared/forgot-password/forgot-password.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

/**
 * @author Alessandro Alberga
 * @description compnent handling use rlogin.
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    '../auth/auth.component.less',
    './login.component.less'
  ]
})
export class LoginComponent implements OnInit {

  /**
   * Login form group reference.
   */
  public loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  constructor(
    private authService: AuthService,
    private router: Router,
    private dialogService: DialogService,
    private snack: MatSnackBar,
    ) { }

  ngOnInit(): void {
    // If user is authenticated send them back to home.
    if (this.authService.userIsAuthenticated()) {
      this.router.navigate(['/home/']);
    }
  }

  /**
   * Login form submission.
   *
   * @param form login form.
   */
  public onSubmit(form): Promise<any> {
    const { value } = form;
    if (form.invalid) { return; }
    return this.authService.login(value.username, value.password)
    .then((res) => {
      if (res && res.token) {
        // We have login, set the session
        AuthService.SetSession(res);
        return this.authService.getMe()
          .then(() => {
            // We have the user, naviate to home.
            this.router.navigate(['/home/']);
          });
      } else {
        // Case of failure.
        this.snack.open('Email or password are incorrect', 'Ok', { duration: 3000 });
      }
    })
    .catch((err) => {
      // Case of error.
      this.snack.open('Email or password are incorrect', 'Ok', { duration: 3000 });
      throw err;
    });
  }

  /**
   * Open forgot password component.
   *
   * @param handle handle to reset password for.
   */
  public openForgotPassword(handle: string): void {
    this.dialogService.openFormDialog({
        formInputs: {
          component: ForgotPasswordComponent,
          title: 'Reset my password',
          icon: 'security'
        },
        componentInputs: {
          handle
        }
      },
      this.resetHandler,
      null,
      { width: '500px' }
    );
  }

  /**
   * Function handler for initiating the password reset process.
   *
   * @param handle the handle from the form dialog res.
   */
  private resetHandler = (handle: string) => {
    if (handle) {
      this.authService.initiatePasswordReset(handle)
      .then(() => {
        this.snack.open(`Reset email sent to ${handle}`, 'Ok', { duration: 3000 });
      })
      .catch(() => {
        this.snack.open(`Something went wrong, please try again.`, 'Ok', { duration: 3000 });
      });
    }
  }
}
