/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { User } from './user.model';
import { GraphQLService } from '../utils/graphQL.service';
import PromiseProvider from '../utils/promiseProvider';
import { AwsS3Service } from '../third-party/aws.s3.service';
import { LocalStorageTokens } from './local-storage.tokens';

const ADMIN_BIT = 1;

/**
 * @author Alessandro Alberga
 * @description Angular service for all authentication functionality. MUST be
 *   provided in root.
 */
@Injectable()
export class AuthService extends PromiseProvider {

  /**
   * User gql string with return fields.
   */
  private userGqlType = `
    id
    username
    email
    first_name
    last_name
    date_of_birth
    tags
    profile_photo
    role_bits
    banner_photo
  `;

  /**
   * Error messages received from login attempts.
   */
  public errors: any = [];

  /**
   * User auth reference of user.
   */
  private _user = new BehaviorSubject<User>(new User({}));

  constructor(
    private qglService: GraphQLService,
    private awsS3Service: AwsS3Service
  ) {
    super();
  }

  /**
   * Set the current session.
   *
   * @param userAuth user auth to set session with.
   */
  public static SetSession(userAuth: UserAuthToken): void {
    localStorage.setItem(LocalStorageTokens.authToken, userAuth.token);
    localStorage.setItem(LocalStorageTokens.authTokenExpiry, JSON.stringify(userAuth.expiry_date));
  }

  /**
   * Listen unto the user.
   */
  get userObservable(): Observable<User> {
    return this._user.asObservable();
  }

  /**
   * Take last user value.
   */
  public getUser(): Promise<User> {
    const user = this.userObservable.pipe(take(1));
    return user.toPromise();
  }

  /**
   * A get me function to get the logged in user's details. On each call it will set the
   * running instance of the user.
   */
  public getMe(): Promise<User> {
    const query = `{
      me {
        ${this.userGqlType}
        subscriptions {
          count
          rows {
            ${this.qglService.subscriptionType}
          }
        }
        subscribers {
          count
          rows {
            ${this.qglService.subscriptionType}
          }
        }
      }
    }`;
    return this.queryToPromise(this.qglService.runQuery(query))
      .then(({ me }) => {
        const user = new User(me);
        this._user.next(user);
        return this.getUser();
      });
  }

  /**
   * Queries the api for a user given a userName input.
   *
   * @param userName unique string identifier of user.
   * @returns promise of a user.
   */
  public async queryUserByUsername(username: string): Promise<User> {
    const query = `{
      findFullUser(username: "${username}") {
        ${this.userGqlType}
        sites {
          id
          uuid
          title
          upvotes {
            count
            rows {
              id
              user {
                id
              }
            }
          }
          downvotes {
            count
            rows {
              id
              user {
                id
              }
            }
          }
        }
        subscriptions {
          count
          rows {
            ${this.qglService.subscriptionType}
          }
        }
        subscribers {
          count
          rows {
            ${this.qglService.subscriptionType}
          }
        }
      }
    }`;
    const res = await this.queryToPromise(this.qglService.runQuery(query));
    return new User(res.findFullUser);
  }

  /**
   * Register a user with the api.
   *
   * @param user user details to register user.
   */
  public register(user: any): Promise<any> {
    const query = `
      mutation Register($user: UserInput!) {
        register(user: $user) {
          success
          reason
        }
      }
    `;
    return this.queryToPromise(this.qglService.runMutation(query, { user }))
      .then(res => res.register);
  }

  /**
   * Update a user with new property values.
   *
   * @param user. is partial so that getter methods aren't required.
   */
  public async updateUser(user: Partial<User>): Promise<User> {
    const mutation = `mutation UpdateUser($new_user_props: UserUpdate!) {
      updateUser(new_user_props: $new_user_props) {
        ${this.userGqlType}
        auth_token
      }
    }`;
    const res = await this.queryToPromise(this.qglService.runMutation(mutation, { new_user_props: user }));
    return res.updateUser;
  }

  /**
   * Reach api endpoint to get an auth token on success.
   *
   * @param hanlde handle to authenticate with.
   * @param password password to authenticate with.
   */
  public login(handle: string, password: string): Promise<UserAuthToken> {
    const query = `
      mutation {
        authenticateUser(handle: "${handle}", password: "${password}") {
          token
          expiry_date
        }
      }
    `;
    return this.queryToPromise(this.qglService.runMutation(query))
      .then(({ authenticateUser }) => authenticateUser);
  }

  /**
   * Initiate the password reset process.
   *
   * @param userHandle user email to send reset instructions to.
   */
  public initiatePasswordReset(userHandle: string): Promise<any> {
    const query = `
      mutation { initiatePasswordRequest(handle: "${userHandle}") }
    `;
    return this.queryToPromise(this.qglService.runMutation(query))
      .then(res => res.initiatePasswordRequest);
  }

  /**
   * Finalise password reset.
   *
   * @param resetToken reset token to verify reset request.
   * @param newPassword new password to set on all user auths.
   */
  public finalisePasswordReset(resetToken: string, newPassword: string): Promise<any> {
    const query = `
      mutation { finalisePasswordRequest(resetToken: "${resetToken}", newPassword: "${newPassword}") }
    `;
    return this.queryToPromise(this.qglService.runMutation(query))
      .then(res => res.finalisePasswordRequest);
  }

  /**
   * Check if the user can access admin console by checking for the admin bit.
   */
  public canAccessAdminConsole(): Promise<boolean> {
    return this.getUser()
      .then(user => {
        // tslint:disable-next-line: no-bitwise
        return ((user.role_bits & ADMIN_BIT) > 0);
      });
  }

  /**
   * Logs user out and reroutes to login.
   */
  public logout(): void {
    // Set logged out session.
    AuthService.SetSession({ token: null, expiry_date: null });
    window.location.href = '/';
    this._user.next(null);
  }

  /**
   * Store the role.
   *
   * @param bits role bits.
   */
  public setRole(bits: number) {
    localStorage.setItem(LocalStorageTokens.bits, JSON.stringify(bits));
  }

  /**
   * Is token expired function.
   */
  private isTokenExpired(): boolean {
    const expiry = this.getExpiryDate();
    if (expiry < new Date()) {
      return true;
    }
    return false;
  }

  /**
   * Public method to flag if the user is authenticated or not.
   */
  public userIsAuthenticated(): boolean {
    const token = this.getToken();
    if (!token) { return false; }
    return !this.isTokenExpired();
  }

  /**
   * Retreive token from local storage.
   */
  public getToken(): string {
    return localStorage.getItem(LocalStorageTokens.authToken);
  }

  /**
   * Get expiry date of token.
   */
  public getExpiryDate(): Date {
    return JSON.parse(localStorage.getItem(LocalStorageTokens.authTokenExpiry));
  }

  /**
   * Upload a file leveraging our third party AWS S3 Service and update the
   * photo column on user.
   */
  public async uploadUserFile(userId: number, file: File, userFileProp: string) {
    // Get the url from the s3 upload func.
    const fileUploadResponse = await this.awsS3Service.uploadUserPhoto(file);
    // Update user with the new url.
    return this.updateUser({ id: userId, [userFileProp]: fileUploadResponse });
  }

  /**
   * Upload file, return url.
   *
   * @param file file to upload.
   */
  public uploadFile(file: File): Promise<string> {
    return this.awsS3Service.uploadUserPhoto(file);
  }
}

/**
 * Simple interface for user auth token.
 */
interface UserAuthToken {
  token: string;
  expiry_date: Date;
}
