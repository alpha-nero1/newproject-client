/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { COMMA, ENTER, BACKSPACE } from '@angular/cdk/keycodes';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatChipInputEvent } from '@angular/material/chips';

/**
 * @author Alessanro Alberga
 * @description Register component.
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../auth/auth.component.less']
})
export class RegisterComponent {
  /**
   * Tags input from mat-chips.
   */
  public tags: string[] = [];

  /**
   * Seperator keys for mat-chips input.
   */
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, BACKSPACE];

  /**
   * Registration form group.
   */
  public registrationForm = new FormGroup(({
    firstname: new FormControl('', [Validators.required]),
    lastname: new FormControl('', [Validators.required]),
    username: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    passwordConfirm: new FormControl('', [Validators.required]),
    dateOfBirth: new FormControl(''),
  }));

  constructor(
    private authService: AuthService,
    private router: Router,
    private snack: MatSnackBar
  ) { }

  /**
   * Add chip to tags.
   *
   * @param event mat chip event containing tag.
   */
  public add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add tag.
    if ((value || '').trim()) {
      this.tags.push(value.trim());
    }

    // Reset the input value.
    if (input) {
      input.value = '';
    }
  }

  /**
   * Remove a tag from the tag list.
   *
   * @param tag tag to remove.
   */
  public remove(tag: string): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  /**
   * Create the new user and send to api.
   *
   * @param form form to register user with.
   */
  public async onSubmit(form: any): Promise<void> {
    const { value } = form;
    // Don't submit if invalid. Give snackbar.
    if (form.invalid) { return; }
    // Pull user from the creation form.
    const newUser = {
      email: value.email,
      first_name: value.firstname,
      last_name: value.lastname,
      username: value.username,
      password: value.password,
      confirm_password: value.passwordConfirm,
      date_of_birth: new Date(value.dateOfBirth),
      tags: this.tags
    };
    // Fire the register.
    const registerResponse = await this.authService.register(newUser);
    if (registerResponse) {
      this.snack.open('User created succesfully!', 'Ok', { duration: 3000 });
      this.router.navigate(['/auth/login']);
    }
  }
}
