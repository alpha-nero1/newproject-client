import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminConsoleComponent } from './admin-console.component';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from '../admin-routing.module';
import { AppAutocompleteModule } from '../../shared/app-autocomplete/app-autocomplete.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { AuthModule } from '../../auth/auth.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from '../../dialog/dialog.module';
import { MatButtonModule } from '@angular/material/button';
import { NpTableModule } from '../../shared/np-table/np-table.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('AdminConsoleComponent', () => {
  let component: AdminConsoleComponent;
  let fixture: ComponentFixture<AdminConsoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminConsoleComponent],
      imports: [
        CommonModule,
        AdminRoutingModule,
        AppAutocompleteModule,
        MatFormFieldModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        AuthModule,
        MatInputModule,
        DialogModule,
        MatButtonModule,
        NpTableModule,
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConsoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
