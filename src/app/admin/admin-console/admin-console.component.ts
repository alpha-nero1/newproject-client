/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/**
 * @author Alessandro Alberga
 * @description The admin console component, smilar to a main menu.
 */
@Component({
  selector: 'app-admin-console',
  templateUrl: './admin-console.component.html',
  styleUrls: ['./admin-console.component.less']
})
export class AdminConsoleComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() { }
}
