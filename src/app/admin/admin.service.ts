/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { Injectable } from '@angular/core';
import PromiseProvider from '../utils/promiseProvider';
import { GraphQLService } from '../utils/graphQL.service';
import { GeneralQueryOpts } from '../shared/types/filter.interface';
import { SiteReportQueryOps } from './types/site-report-query-ops.interface';
import { Site } from '../site/site.model';
import { SiteReport, SiteReportGql } from './types/site-report.interface';
import { DailyFeature, DailyFeatureGql } from './types/daily-feature.interface';

/**
 * @author Alessandro Alberga
 * @description Admin service manager.
 */
@Injectable()
export class AdminService extends PromiseProvider {

  constructor(private gqlService: GraphQLService) {
    super();
  }

  /**
   * Get site reports according to options.
   *
   * @param options options to query site reports with.
   */
  public getSiteReports(opts: SiteReportQueryOps): Promise<{ count: number, rows: SiteReport[] }> {
    const gql = `
      query ($opts: ListReportedSitesParams) {
        getSiteReports(opts: $opts) {
          count
          rows {
            ${SiteReportGql}
          }
        }
      }
    `;
    return this.queryToPromise(this.gqlService.runQuery(gql, { opts }))
    .then(res => res.getSiteReports);
  }

  /**
   * Get sites that have been reported according to options.
   *
   * @param opts general query options.
   */
  public getReportedSites(opts: ListReportedSitesParams): Promise<{ count: number, rows: Site[] }> {
    const endpoint = 'getReportedSites';
    const gql = `
      query ($opts: ListReportedSitesParams) {
        ${endpoint}(opts: $opts) {
          count
          rows {
            id
            uuid
            title
            reports {
              id
              reason
              reporting_user_id
            }
            site_passed_threshold
            user {
              id
              first_name
              last_name
              username
            }
          }
        }
      }
    `;
    return this.queryToPromise(this.gqlService.runQuery(gql, { opts }))
    .then(res => res[endpoint]);
  }

  /**
   * Dismiss reports.
   *
   * @param siteReportIds ids to dismiss.
   */
  public dismissReports(siteReportIds: number[]): Promise<SiteReport[]> {
    const endpoint = 'dismissSiteReports';
    const gql = `
      mutation ($site_report_ids: [Int]!) {
        ${endpoint}(site_report_ids: $site_report_ids) {
          id
        }
      }
    `;
    return this.queryToPromise(this.gqlService.runMutation(gql, {
      site_report_ids: siteReportIds
    }))
    .then(res => res[endpoint]);
  }

  /**
   * Create new daily feature.
   *
   * @param feature new daily feature to create.
   */
  public createDailyFeature(feature: DailyFeature): Promise<SiteReport[]> {
    const endpoint = 'createDailyFeature';
    const gql = `
      mutation ($daily_feature: DailyFeatureInput!) {
        ${endpoint}(daily_feature: $daily_feature) {
          id
        }
      }
    `;
    return this.queryToPromise(this.gqlService.runMutation(gql, {
      daily_feature: feature
    }))
    .then(res => res[endpoint]);
  }
}

export interface ListReportedSitesParams extends GeneralQueryOpts {
  site_id?: number;
  after_date?: Date;
  before_date?: Date;
}
