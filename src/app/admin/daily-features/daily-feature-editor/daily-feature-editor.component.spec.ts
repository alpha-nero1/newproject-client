import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyFeatureEditorComponent } from './daily-feature-editor.component';

import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { NpTableModule } from '../../../shared/np-table/np-table.module';
import { AdminGuard } from '../../admin.guard';
import { AdminService } from '../../admin.service';
import { MatButtonModule } from '@angular/material/button';
import { DialogModule } from '../../../dialog/dialog.module';
import { AuthModule } from '../../../auth/auth.module';
import { AppAutocompleteModule } from '../../../shared/app-autocomplete/app-autocomplete.module';
import { AdminRoutingModule } from '../../admin-routing.module';
import { ApolloTestingModule } from 'apollo-angular/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('DailyFeatureEditorComponent', () => {
  let component: DailyFeatureEditorComponent;
  let fixture: ComponentFixture<DailyFeatureEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DailyFeatureEditorComponent],
      imports: [
        BrowserAnimationsModule,
        CommonModule,
        AdminRoutingModule,
        AppAutocompleteModule,
        MatFormFieldModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        AuthModule,
        MatInputModule,
        DialogModule,
        MatButtonModule,
        NpTableModule,
        ApolloTestingModule
      ],
      providers: [
        AdminGuard,
        AdminService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyFeatureEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
