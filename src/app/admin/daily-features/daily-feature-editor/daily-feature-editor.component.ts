import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DailyFeature } from '../../types/daily-feature.interface';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { AppAutocompleteDelegate } from '../../../shared/app-autocomplete/types/app-autocomplete-delegate.model';
import { Site } from '../../../site/site.model';
import { SitesService } from '../../../site/sites.service';

@Component({
  selector: 'app-daily-feature-editor',
  templateUrl: './daily-feature-editor.component.html',
  styleUrls: ['./daily-feature-editor.component.less']
})
export class DailyFeatureEditorComponent implements OnInit {

  private activeRouteSub: Subscription;

  private dailyFeature: DailyFeature;

  public dailyFeatureForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    button_text: new FormControl('', [Validators.required]),
    site_id: new FormControl(null, [Validators.required])
  });

  /**
   * Delegate for the sites autocomplete.
   */
  public sitesAutocompleteDelegate = new AppAutocompleteDelegate<Site>({
    displayProperties: ['title']
  });

  constructor(private siteService: SitesService) {
    // Properly setup sites autocomplete
    this.sitesAutocompleteDelegate.debounce = 200;
    this.sitesAutocompleteDelegate.filterOverrideHandler = this.filterSiteOverride;
  }

  ngOnInit(): void { }

  /**
   * Filter sites override.
   */
  private filterSiteOverride: (v: string) => Promise<Site[]> = (term: string) => {
    return this.siteService.searchSites(term, [], {})
    .then(res => res.rows);
  }

  /**
   * Get site control.
   */
  public get siteControl(): any {
    return this.dailyFeatureForm.get('site_id');
  }

  /**
   * Submit handle.
   */
  public submit(): Promise<any> {
    return new Promise((resolve) => {
      if (this.dailyFeatureForm.valid) {
        const submissionVals = {
          ...this.dailyFeatureForm.value
        };
        delete submissionVals.site_id;
        submissionVals.site_id = this.sitesAutocompleteDelegate.mapSelectedToPks();
        resolve(submissionVals);
      }
    });
  }
}
