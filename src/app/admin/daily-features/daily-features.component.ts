/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { Component, OnInit } from '@angular/core';
import { NPTableObject } from '../../shared/np-table/types/np-table-object.model';
import { GeneralQueryOpts } from '../../shared/types/filter.interface';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';
import { DialogService } from '../../dialog/dialog.service';
import { DailyFeatureEditorComponent } from './daily-feature-editor/daily-feature-editor.component';
import { SitesService } from '../../site/sites.service';

/**
 * @author Alessandro Alberga
 * @description daily features list component.
 */
@Component({
  selector: 'app-daily-features',
  templateUrl: './daily-features.component.html',
  styleUrls: ['./daily-features.component.less']
})
export class DailyFeaturesComponent implements OnInit {

  public featuresTableObj = new NPTableObject<any>({
    columns: [
      {
        header: 'Site name',
        property: 'siteName'
      },
      {
        header: 'Site user',
        property: 'siteUser'
      },
      {
        header: 'Feature title',
        property: 'title'
      },
      {
        header: 'Button text',
        property: 'button_text'
      },
      {
        header: 'Active',
        property: 'isActive'
      }
    ]
  });

  private queryOpts: GeneralQueryOpts = { };

  constructor(
    private router: Router,
    private adminService: AdminService,
    private dialogService: DialogService,
    private siteService: SitesService
  ) { }

  ngOnInit(): void {
    this.getDailyFeatures();
  }

  /**
   * Get and process daily feature results.
   */
  private getDailyFeatures() {
    this.featuresTableObj.loading = true;
    return this.siteService.getDailyFeatures(this.queryOpts)
    .then(({ count, rows }) => {
      this.featuresTableObj.setCount(count);
      this.featuresTableObj.setData(this.processData(rows));
    })
    .finally(() => {
      this.featuresTableObj.loading = false;
    });
  }

  private processData = rows => (
    rows.map((rec, i) => ({
      ...rec,
      isActive: (i === 0),
      siteUser: rec?.site?.user?.username,
      siteName: rec?.site?.title
    }))
  )

  /**
   * Open the create new feature form component.
   */
  public openNewFeatureForm(): void {
    this.dialogService.openFormDialog({
      formInputs: {
        component: DailyFeatureEditorComponent
      }
    }, res => {
      if (!res) { return false; }
      // Create new and refresh list.
      return this.adminService.createDailyFeature(res)
      .then(newFeature => {
        this.getDailyFeatures();
      });
    });
  }
}
