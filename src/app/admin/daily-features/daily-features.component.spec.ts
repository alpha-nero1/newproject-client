import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyFeaturesComponent } from './daily-features.component';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from '../admin-routing.module';
import { AppAutocompleteModule } from '../../shared/app-autocomplete/app-autocomplete.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthModule } from '../../auth/auth.module';
import { MatInputModule } from '@angular/material/input';
import { DialogModule } from '../../dialog/dialog.module';
import { MatButtonModule } from '@angular/material/button';
import { NpTableModule } from '../../shared/np-table/np-table.module';
import { RouterTestingModule } from '@angular/router/testing';
import { AdminService } from '../admin.service';
import { GraphQLService } from '../../utils/graphQL.service';
import { ApolloTestingModule } from 'apollo-angular/testing';

describe('DailyFeaturesComponent', () => {
  let component: DailyFeaturesComponent;
  let fixture: ComponentFixture<DailyFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DailyFeaturesComponent],
      imports: [
        ApolloTestingModule,
        CommonModule,
        AdminRoutingModule,
        AppAutocompleteModule,
        MatFormFieldModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        AuthModule,
        MatInputModule,
        DialogModule,
        MatButtonModule,
        NpTableModule,
        RouterTestingModule
      ],
      providers: [
        AdminService,
        GraphQLService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
