/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 */
export interface SiteReport {
  /**
   * Id of site report.
   */
  id?: number;
  /**
   * User who reported site.
   */
  reporting_user_id?: number;
  /**
   * Reason of the report.
   */
  reason?: string;
  /**
   * The site reported.
   */
  site_id?: number;
  /**
   * Included site model.
   */
  site?: any;
  /**
   * Reporting user.
   */
  reporting_user?: any;
  /**
   * Indicator that report is dismissed.
   */
  dismissed?: boolean;
}

// Accompanying site report gql.
export const SiteReportGql = `
  id
  reporting_user_id
  reason
  site_id
  site {
    id
    title
    user_id
  }
  reporting_user {
    id
    first_name
    last_name
    username
    email
  }
  dismissed
`;
