/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Site } from '../../site/site.model';

/**
 * @author Alessandro Alberga
 * @description Daily feature interface descriptor.
 */
export interface DailyFeature {

  /**
   * Id of feature.
   */
  id?: number;

  /**
   * Title to accompany site name.
   */
  title?: string;

  /**
   * Text to go on button.
   */
  button_text?: string;

  /**
   * Id of featured site.
   */
  site_id?: number;

  /**
   * Disabled.
   */
  disabled?: boolean;

  /**
   * Created at.
   */
  created_at?: Date;

  /**
   * Updated at.
   */
  updated_at?: Date;

  /**
   * Included site model.
   */
  site?: Site;
}

export const DailyFeatureGql = `
  id
  title
  button_text
  site_id
  site {
    id
    uuid
    title
    user {
      id
      first_name
      last_name
      username
      email
    }
  }
  disabled
  created_at
  updated_at
`;
