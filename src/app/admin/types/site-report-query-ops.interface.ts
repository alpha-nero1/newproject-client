/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { GeneralQueryOpts } from '../../shared/types/filter.interface';

/**
 * @author Alessandro Alberga
 */
export interface SiteReportQueryOps extends GeneralQueryOpts {
  site_id?: number;
  created_after?: Date;
  created_before?: Date;
  reporting_user_id?: number;
}
