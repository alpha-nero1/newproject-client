/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit } from '@angular/core';
import { UserAware } from '../../common/user-aware.component';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';

/**
 * @author Alessandro Alberga
 * @description administration main component.
 */
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.less']
})
export class AdminComponent extends UserAware {

  constructor(authService: AuthService, private router: Router) {
    super(authService);
    this.initCallback((usr) => null);
  }

  /**
   * Simply navigate to the report manager.
   */
  public gotoReportManager(): void {
    this.router.navigate(['/admin/report-manager']);
  }

  /**
   * Simply navigate to home.
   */
  public gotoHome(): void {
    this.router.navigate(['/home/']);
  }

  /**
   * Go to daily features.
   */
  public gotoDailyFeatures(): void {
    this.router.navigate(['/admin/daily-features']);
  }
}
