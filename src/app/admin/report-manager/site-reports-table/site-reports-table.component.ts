/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Site } from '../../../site/site.model';
import { AdminService } from '../../admin.service';
import { NPTableObject } from '../../../shared/np-table/types/np-table-object.model';
import { SiteReportQueryOps } from '../../types/site-report-query-ops.interface';
import { DialogService } from '../../../dialog/dialog.service';

/**
 * @author Alessandro Alberga
 * @description Site reports table component.
 */
@Component({
  selector: 'app-site-reports-table',
  templateUrl: './site-reports-table.component.html',
  styleUrls: ['./site-reports-table.component.less']
})
export class SiteReportsTableComponent implements OnInit, OnDestroy {

  /**
   *  Site object containing all the reports.
   */
  private site: Site;

  /**
   * Router swubscription for vars.
   */
  private activeRouteSub: Subscription;

  /**
   * Site reports table obj.
   */
  public siteReportsTableObj = new NPTableObject<any>({
    columns: [
      {
        header: 'Site id',
        property: 'siteId'
      },
      {
        header: 'Site name',
        property: 'siteName'
      },
      {
        header: 'Reporting user',
        property: 'reportingUser'
      },
      {
        header: 'Reason',
        property: 'reason'
      }
    ]
  });

  /**
   * Query options.
   */
  public queryOpts: SiteReportQueryOps = { };

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private adminService: AdminService,
    private dialogService: DialogService
  ) { }

  ngOnInit(): void {
    this.siteReportsTableObj.rowClickHandler = this.rowClick;
    this.activeRouteSub = this.activatedRoute.params.subscribe(params => {
      const siteId = +params.id;
      this.queryOpts.site_id = siteId;
      this.getSiteReports();
    });
  }

  /**
   * Get the site reports.
   */
  private getSiteReports(): Promise<any> {
    this.siteReportsTableObj.loading = true;
    return this.adminService.getSiteReports(this.queryOpts)
    .then(siteReports => {
      this.siteReportsTableObj.setCount(siteReports.count);
      this.siteReportsTableObj.setData(this.processRows(siteReports.rows));
    })
    .finally(() => {
      this.siteReportsTableObj.loading = false;
    });
  }

  /**
   * Process the rows to show in the table.
   */
  private processRows = (siteReport) => siteReport.map(sr => ({
    ...sr,
    reportingUser: sr?.reporting_user?.username,
    siteId: sr?.site?.id,
    siteName: sr?.site?.title
  }))

  /**
   * Go back to the reported sites.
   */
  public goBack() {
    this.router.navigate(['/admin/report-manager']);
  }

  /**
   * Dismiss report on row click handler.
   */
  public rowClick = (row) => {
    this.dialogService.openConfirmation((res) => {
      if (res) {
        this.adminService.dismissReports([row.id])
        .then(() => {
          this.getSiteReports();
        });
      }
    }, null, {
      data: {
        title: 'Dismiss report?'
      }
    });
  }

  ngOnDestroy(): void {
    if (this.activatedRoute) {
      this.activeRouteSub.unsubscribe();
    }
  }
}
