import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteReportsTableComponent } from './site-reports-table.component';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from '../../admin-routing.module';
import { AppAutocompleteModule } from '../../../shared/app-autocomplete/app-autocomplete.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthModule } from '../../../auth/auth.module';
import { MatInputModule } from '@angular/material/input';
import { DialogModule } from '../../../dialog/dialog.module';
import { MatButtonModule } from '@angular/material/button';
import { NpTableModule } from '../../../shared/np-table/np-table.module';
import { AdminGuard } from '../../admin.guard';
import { AdminService } from '../../admin.service';
import { RouterTestingModule } from '@angular/router/testing';
import { ApolloTestingModule } from 'apollo-angular/testing';

describe('SiteReportsTableComponent', () => {
  let component: SiteReportsTableComponent;
  let fixture: ComponentFixture<SiteReportsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SiteReportsTableComponent],
      imports: [
        CommonModule,
        AdminRoutingModule,
        AppAutocompleteModule,
        MatFormFieldModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        AuthModule,
        MatInputModule,
        DialogModule,
        MatButtonModule,
        NpTableModule,
        RouterTestingModule,
        ApolloTestingModule
      ],
      providers: [
        AdminGuard,
        AdminService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteReportsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
