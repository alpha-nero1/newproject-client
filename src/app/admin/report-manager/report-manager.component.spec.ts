import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from '../admin-routing.module';
import { AppAutocompleteModule } from '../../shared/app-autocomplete/app-autocomplete.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthModule } from '../../auth/auth.module';
import { MatInputModule } from '@angular/material/input';
import { DialogModule } from '../../dialog/dialog.module';
import { MatButtonModule } from '@angular/material/button';
import { NpTableModule } from '../../shared/np-table/np-table.module';
import { ReportManagerComponent } from './report-manager.component';
import { AdminService } from '../admin.service';
import { ApolloTestingModule } from 'apollo-angular/testing';
import { GraphQLService } from '../../utils/graphQL.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('ReportManagerComponent', () => {
  let component: ReportManagerComponent;
  let fixture: ComponentFixture<ReportManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReportManagerComponent],
      imports: [
        CommonModule,
        AdminRoutingModule,
        AppAutocompleteModule,
        MatFormFieldModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        AuthModule,
        MatInputModule,
        DialogModule,
        MatButtonModule,
        NpTableModule,
        ApolloTestingModule,
        RouterTestingModule
      ],
      providers: [
        AdminService,
        ApolloTestingModule,
        GraphQLService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
