/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { NPTableObject } from '../../shared/np-table/types/np-table-object.model';
import { Site } from '../../site/site.model';
import { Router } from '@angular/router';

/**
 * @author Alessandro Alberga
 * @description Report manager component where admins can dismiss reports or block sites.
 */
@Component({
  selector: 'app-report-manager',
  templateUrl: './report-manager.component.html',
  styleUrls: ['./report-manager.component.less']
})
export class ReportManagerComponent implements OnInit {

  /**
   * Use our special table object to display results.
   * We are really listing just sites, however, in the context of reports.
   */
  public reportsTableObj = new NPTableObject<Site>({
    clickable: true,
    columns: [
      {
        property: 'id',
        header: 'Site id'
      },
      {
        property: 'title',
        header: 'Site'
      },
      {
        property: 'username',
        header: 'Site owner'
      },
      {
        property: 'topFiveReasons',
        header: 'Reasons (top 5)'
      },
      {
        property: 'reportCount',
        header: 'Report count'
      },
      {
        property: 'site_passed_threshold',
        header: 'Passed threshold'
      }
    ]
  });

  public queryOpts = { };

  constructor(private adminService: AdminService, private router: Router) { }

  ngOnInit(): void {
    this.reportsTableObj.rowClickHandler = this.rowClick;
    this.getReports();
  }

  /**
   * Fetch reports from the api using the active filter configuration.
   */
  private getReports(): Promise<any> {
    this.reportsTableObj.loading = false;
    return this.adminService.getReportedSites(this.queryOpts)
    .then((res) => {
      this.reportsTableObj.setData(this.processRows(res.rows));
      this.reportsTableObj.setCount(res.count);
      return res;
    })
    .finally(() => this.reportsTableObj.loading = false);
  }

  /**
   * Row click handler implementation.
   */
  private rowClick = (row) => {
    this.router.navigate([`/admin/report-manager/sites/${row.id}`]);
  }

  /**
   * Process rows.
   *
   * @param rows rows to process.
   */
  private processRows(rows: any[]): any[] {
    return rows.map(row => ({
      ...row,
      topFiveReasons: row.reports.filter((r, i) => i < 5).map(repRow => repRow.reason),
      reportCount: row?.reports?.length,
      username: row?.user?.username,
      site_passed_threshold: row?.site_passed_threshold || false
    }));
  }
}
