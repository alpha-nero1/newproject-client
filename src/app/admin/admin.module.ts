/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthModule } from '../auth/auth.module';
import { ReportManagerComponent } from './report-manager/report-manager.component';
import { AdminConsoleComponent } from './admin-console/admin-console.component';
import { NpTableModule } from '../shared/np-table/np-table.module';
import { AdminGuard } from './admin.guard';
import { AdminService } from './admin.service';
import { MatInputModule } from '@angular/material/input';
import { SiteReportsTableComponent } from './report-manager/site-reports-table/site-reports-table.component';
import { MatButtonModule } from '@angular/material/button';
import { DialogModule } from '../dialog/dialog.module';
import { DailyFeaturesComponent } from './daily-features/daily-features.component';
import { DailyFeatureEditorComponent } from './daily-features/daily-feature-editor/daily-feature-editor.component';
import { AppAutocompleteModule } from '../shared/app-autocomplete/app-autocomplete.module';
import { MatFormFieldModule } from '@angular/material/form-field';

/**
 * @author Alessandro Alberga
 * @description admin module.
 */
@NgModule({
  declarations: [
    AdminComponent,
    ReportManagerComponent,
    AdminConsoleComponent,
    SiteReportsTableComponent,
    DailyFeaturesComponent,
    DailyFeatureEditorComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    AppAutocompleteModule,
    MatFormFieldModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    AuthModule,
    MatInputModule,
    DialogModule,
    MatButtonModule,
    NpTableModule
  ],
  providers: [
    AdminGuard,
    AdminService
  ]
})
export class AdminModule { }
