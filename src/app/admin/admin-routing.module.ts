/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { ReportManagerComponent } from './report-manager/report-manager.component';
import { AdminConsoleComponent } from './admin-console/admin-console.component';
import { AdminGuard } from './admin.guard';
import { SiteReportsTableComponent } from './report-manager/site-reports-table/site-reports-table.component';
import { DailyFeaturesComponent } from './daily-features/daily-features.component';
import { DailyFeatureEditorComponent } from './daily-features/daily-feature-editor/daily-feature-editor.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'console'
      },
      {
        path: 'console',
        component: AdminConsoleComponent
      },
      {
        path: 'report-manager',
        component: ReportManagerComponent
      },
      {
        path: 'daily-features',
        component: DailyFeaturesComponent
      },
      {
        path: 'report-manager/sites/:id',
        component: SiteReportsTableComponent
      }
    ]
  }
];

/**
 * @author Alessandro Alberga
 * @description admin routing module.
 */
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
