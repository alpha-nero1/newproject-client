/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

/**
 * @author Alessandro Alberga
 * @description main module component. is the main entry component for most of our modules.
 */
@Component({
  selector: 'app-main-module',
  templateUrl: './main-module.component.html',
})
export class MainModuleComponent { }
