/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainModuleComponent } from './main-module/main-module.component';
import { NavbarModule } from '../navbar/navbar.module';
import { FooterModule } from '../footer/footer.module';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

/**
 * @author Alessandro Alberga
 * @description main module component module wrapper.
 */
@NgModule({
  declarations: [MainModuleComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FooterModule,
    NavbarModule,
    RouterModule
  ],
  exports: [MainModuleComponent],
})
export class MainModuleComponentModule {}
