/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { StandardDatabaseRecord } from '../../common/types/standard-database-record.interface';

/**
 * @author Alessandro Alberga
 * @description User activity interface description.
 */
export interface Notification extends StandardDatabaseRecord {
  /**
   * Title of activity.
   */
  title?: string;

  /**
   * Activity icon.
   */
  icon?: string;

  /**
   * Slug action of activity.
   */
  slug?: string;

  /**
   * Id of user.
   */
  user_id: number;
}

// Accompanying query options.
export const NotificationGql = `
  id
  title
  slug
  icon
  created_at
  read_time
`;
