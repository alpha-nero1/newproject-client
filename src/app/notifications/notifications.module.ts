/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NpTableModule } from '../shared/np-table/np-table.module';
import { MainModuleComponentModule } from '../main-module-component/main-module-component.module';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ToMarkupModule } from '../common/pipes/to-markup/to-markup.module';
import { NotificationsComponent } from './notifications/notifications.component';
import { NotificationService } from './notifications.service';
import { NotificationRoutingModule } from './notifications-routing.module';

/**
 * @author Alessandro Alberga
 */
@NgModule({
  declarations: [NotificationsComponent],
  imports: [
    NotificationRoutingModule,
    CommonModule,
    MainModuleComponentModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule,
    NpTableModule,
    ToMarkupModule
  ],
  providers: [
    NotificationService
  ]
})
export class NotificationModule { }
