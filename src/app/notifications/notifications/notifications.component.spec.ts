/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NotificationsComponent } from './notifications.component';
import { NotificationRoutingModule } from '../notifications-routing.module';
import { CommonModule } from '@angular/common';
import { MainModuleComponentModule } from '../../main-module-component/main-module-component.module';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NpTableModule } from '../../shared/np-table/np-table.module';
import { ToMarkupModule } from '../../common/pipes/to-markup/to-markup.module';
import { NotificationService } from '../notifications.service';
import { GraphQLService } from '../../utils/graphQL.service';
import { ApolloTestingModule } from 'apollo-angular/testing';

describe('NotificationsComponent', () => {
  let component: NotificationsComponent;
  let fixture: ComponentFixture<NotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NotificationsComponent],
      imports: [
        NotificationRoutingModule,
        CommonModule,
        MainModuleComponentModule,
        MatListModule,
        MatIconModule,
        MatButtonModule,
        FlexLayoutModule,
        NpTableModule,
        ApolloTestingModule,
        ToMarkupModule
      ],
      providers: [
        NotificationService,
        GraphQLService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
