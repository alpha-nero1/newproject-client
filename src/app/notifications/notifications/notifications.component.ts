/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit } from '@angular/core';
import { Notification } from '../types/notification.interface';
import { Utils } from '../../utils/utils';
import { NotificationService } from '../notifications.service';
import { PagedDatastoreDelegate } from '../../common/paged-datastore-delegate/paged-datastore-delegate';

/**
 * @author Alessandro Alberga
 * @description Activity component.
 */
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.less']
})
export class NotificationsComponent implements OnInit {

  /**
   * Notifications paged data store.
   */
  public notificationsPagedDataStore = new PagedDatastoreDelegate<Notification>({
    loadResultsCallback: (opts) => this.notificationsService.getNotifications(opts),
    createdAtFormatterFunction: (date) => Utils.GetTodayConsideredDate(date)
  });

  constructor(private notificationsService: NotificationService) { }

  ngOnInit(): void {
    this.notificationsPagedDataStore.loadResults(0);
  }
}
