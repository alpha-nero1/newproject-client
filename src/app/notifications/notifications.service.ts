/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { Injectable } from '@angular/core';
import PromiseProvider from '../utils/promiseProvider';
import { GraphQLService } from '../utils/graphQL.service';
import { GeneralQueryOpts } from '../shared/types/filter.interface';
import { NotificationGql, Notification } from './types/notification.interface';

/**
 * @author Alessandro Alberga
 * @description Service managing the activity module API calls.
 */
@Injectable()
export class NotificationService extends PromiseProvider {
  constructor(private gqlService: GraphQLService) {
    super();
  }

  /**
   * Get sites that the user has votes on.
   *
   * @param up up or down boolean param.
   * @param page page to retieve.
   */
  public getNotifications(opts: GeneralQueryOpts): Promise<{ count: number, rows: Notification[] }> {
    const endpoint = 'getNotifications';
    const gql = `query ($opts: GeneralQueryOpts!) {
      ${endpoint}(opts: $opts) {
        count
        rows {
          ${NotificationGql}
        }
      }
    }
    `;
    return this.queryToPromise(this.gqlService.runQuery(gql, { opts }))
      .then((res) => res[endpoint]);
  }
}
