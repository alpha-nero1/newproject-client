/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description interface all forms that are to be injected into a
 * dialog are to implement. This ensures they have required properties.
 */
export interface InjectedFormDialog {

  /**
   * Submit hander needs to return a promise with the input from
   * the user.
   */
  submit(): Promise<any>;
}
