/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormDialogComponent } from './form-dialog/form-dialog.component';
import { InjectedFormData } from './injected-form-data.model';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

/**
 * @author Alessandro Alberga
 * @description this injectable dialog service let's us open dialogs to get
 *   inputs from the user.
 */
@Injectable()
export class DialogService implements OnDestroy {

  /**
   * Reference to the subscription to avoid memory leaks.
   */
  private dialogRefSubscription: Subscription;

  constructor(private dialog: MatDialog, private snackbar: MatSnackBar) { }

  /**
   * Open a form dialog, provide a callback to do something with the data.
   *
   * @param data data to inject contains inputs and the component.
   * @param submissionHandler function to execute on form submission.
   * @param errorHandler function to execute on failure.
   */
  public openFormDialog(
    data: InjectedFormData,
    submissionHandler: any,
    errorHandler?: any,
    config?: MatDialogConfig
    ) {
    const formNonNegotiables: MatDialogConfig = {
      minWidth: '300px'
    };
    const formConfig = { ...config, ...formNonNegotiables, data };
    const dialogRef = this.dialog.open(FormDialogComponent, formConfig);
    const errHandler = errorHandler ? errorHandler : (() => null);
    this.dialogRefSubscription = dialogRef.afterClosed().subscribe(submissionHandler);
  }

  /**
   * Open confitmation dialog. Retreives a STRICT yes or no answer.
   *
   * @param submissionHandler the handler for when the dialog is submitted.
   * @param errorHandler option for error handler func.
   * @param config ibjectable configuration.
   */
  public openConfirmation(
    submissionHandler: (answer: boolean) => void,
    errorHandler?: any,
    config?: any
  ): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, config);
    const errHandler = errorHandler ? errorHandler : (() => null);
    this.dialogRefSubscription = dialogRef.afterClosed().subscribe(submissionHandler, errHandler);
  }

  ngOnDestroy(): void {
    if (this.dialogRefSubscription) {
      this.dialogRefSubscription.unsubscribe();
    }
  }
}
