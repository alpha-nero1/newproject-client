/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description defines what the data that is given to form-dialog-component
 *   should look like.
 */
export interface InjectedFormData {
  /**
   * Input variables to inject into the component via the instance variable.
   */
  componentInputs?: any;

  /**
   * Inputs to configure the form.
   */
  formInputs: FormInputs;
}

/**
 * Form inputs interface.
 */
interface FormInputs {

  /**
   * Component required to inject into the form.
   */
  component: any;

  /**
   * Optional text for the close button.
   */
  closeText?: string;

  /**
   * Optional text for the submit text.
   */
  submitText?: string;

  /**
   * Optional text for the title.
   */
  title?: string;

  /**
   * Icon to use in form header.
   */
  icon?: string;
}
