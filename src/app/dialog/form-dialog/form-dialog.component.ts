/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import {
  Component,
  OnInit,
  ViewContainerRef,
  ViewChild,
  ComponentFactoryResolver,
  Inject,
  ComponentRef
} from '@angular/core';
import { InjectedFormData } from 'src/app/dialog/injected-form-data.model';
import { Utils } from '../../utils/utils';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

/**
 * @author Alessandro Alberga
 * @description all purpose form dialog component.
 */
@Component({
  selector: 'app-form-dialog',
  templateUrl: './form-dialog.component.html',
  styleUrls: ['./form-dialog.component.less']
})
export class FormDialogComponent implements OnInit {

  /**
   * Title of the form component.
   */
  public title: string;

  /**
   * Icon for the form title.
   */
  public icon: string;

  /**
   * Text of the close button.
   */
  public closeText = 'close';

  /**
   * Text of the submit button.
   */
  public submitText = 'submit';

  /**
   * Component code to instantiate.
   */
  public component: any;

  /**
   * Form viewchild.
   */
  @ViewChild('form', { static: true, read: ViewContainerRef }) formContainer: ViewContainerRef;

  /**
   * Generic component reference.
   */
  private componentRef: ComponentRef<any>;

  constructor(
    public dialogRef: MatDialogRef<FormDialogComponent>,
    private componentFactoryResolver: ComponentFactoryResolver,
    @Inject(MAT_DIALOG_DATA) public data: InjectedFormData
  ) { }

  ngOnInit(): void {
    this.loadInputVariablesFromData();
    this.loadComponent();
  }

  /**
   * Put data into our injectable input variables and
   * use them programatically.
   */
  private loadInputVariablesFromData() {
    const inputs = this.data.formInputs;
    Object.keys(inputs).forEach(key => {
      this[key] = inputs[key];
    });
  }

  /**
   * Load the compoennt in the form and set its data.
   */
  private loadComponent(): void {
    // Setup the factory to dynamically create the component.
    const factory = this.componentFactoryResolver.resolveComponentFactory(this.component);
    // Hey brother, create component!
    this.componentRef = this.formContainer.createComponent(factory);
    // Set the input variables of injected component.
    this.loadComponentInstanceVariablesFromData();
  }

  /**
   * Put data into our injectable input variables and
   * use them programatically.
   */
  public loadComponentInstanceVariablesFromData(): void {
    if (this.data.componentInputs) {
      Object.keys(this.data.componentInputs).forEach(key => {
        this.componentRef.instance[key] = this.data.componentInputs[key];
      });
    }
  }

  /**
   * Close the dialog when we click ~ `no`.
   */
  onNoClick(): void {
    this.dialogRef.close();
  }

  /**
   * Handle the submit of the form.
   */
  public onSubmit(): void {
    // Reach into the instance and call its submit.
    this.componentRef.instance.submit()
      .then(res => this.dialogRef.close(res))
      .catch(err => this.dialogRef.close(err));
  }
}
