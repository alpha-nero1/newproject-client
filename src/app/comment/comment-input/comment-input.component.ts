/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, EventEmitter, Output, Input, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Comment } from '../types/comment.model';

/**
 * @author Alessandro Alberga
 * @description component to take simple text input from the user.
 */
@Component({
  selector: 'app-comment-input',
  templateUrl: './comment-input.component.html',
  styleUrls: ['./comment-input.component.less']
})
export class CommentInputComponent implements AfterViewInit {

  /**
   * Reference of comment input.
   */
  @ViewChild('commentInput') commentInput: ElementRef;

  /**
   * Placeholder input.
   */
  @Input() placeholder: string;

  /**
   * Allow scroll to prevention.
   */
  @Input() preventScrollOnInit: boolean;

  /**
   * Simple output emitter for when user clicks on the send icon.
   */
  @Output() submitMessage = new EventEmitter<string>();

  /**
   * Parent comment.
   */
  @Input() set parentComment(comment: Comment) {
    if (comment) {
      this.placeholder = `Replying to ${comment?.user?.username}...`;
    }
  }

  /**
   * Message that the user is writing.
   */
  public message = '';

  ngAfterViewInit(): void {
    if (!this.preventScrollOnInit) {
      // Effectively scroll into view on init.
      this.commentInput.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'nearest' });
    }
  }

  /**
   * Emit that the send was clicked.
   */
  public onSendMessage(): void {
    if (this.message && this.message.length) {
      this.submitMessage.emit(this.message);
      this.message = '';
    }
  }
}
