/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsViewComponent } from './comments-view/comments-view.component';
import { CommentService } from './comment.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CommentInputComponent } from './comment-input/comment-input.component';
import { CommentComponent } from './comment/comment.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { VoteModule } from '../shared/vote/vote.module';
import { MatButtonModule } from '@angular/material/button';
import { UserAvatarModule } from '../shared/user-avatar/user-avatar.module';

/**
 * @author Alessandro Alberga
 * @description Comment module.
 */
@NgModule({
  declarations: [
    CommentsViewComponent,
    CommentInputComponent,
    CommentComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    UserAvatarModule,
    MatFormFieldModule,
    RouterModule,
    VoteModule
  ],
  providers: [
    CommentService
  ],
  exports: [
    CommentsViewComponent
  ]
})
export class CommentModule { }
