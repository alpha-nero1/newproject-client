import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentsViewComponent } from './comments-view.component';
import { RouterModule } from '@angular/router';
import { VoteModule } from '../../shared/vote/vote.module';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { UserAvatarModule } from '../../shared/user-avatar/user-avatar.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AuthService } from '../../auth/auth.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GraphQLService } from '../../utils/graphQL.service';
import { ApolloTestingModule } from 'apollo-angular/testing';
import { AwsS3Service } from '../../third-party/aws.s3.service';
import { CommentService } from '../comment.service';

describe('CommentsViewComponent', () => {
  let component: CommentsViewComponent;
  let fixture: ComponentFixture<CommentsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentsViewComponent],
      imports: [
        CommonModule,
        FlexLayoutModule,
        FormsModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        UserAvatarModule,
        MatFormFieldModule,
        RouterModule,
        VoteModule,
        ApolloTestingModule
      ],
      providers: [
        AuthService,
        AwsS3Service,
        CommentService,
        GraphQLService,
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: [] }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
