/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, Input, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { Comment } from '../types/comment.model';
import { PagedDatastoreDelegate } from '../../common/paged-datastore-delegate/paged-datastore-delegate';
import { Subscription } from 'rxjs';
import { CommentService } from '../comment.service';
import { UserAware } from '../../common/user-aware.component';
import { AuthService } from '../../auth/auth.service';

/**
 * @author Alessandro Alberga
 * @description View encapsulating the comments view.
 */
@Component({
  selector: 'app-comments-view',
  templateUrl: './comments-view.component.html',
  styleUrls: ['./comments-view.component.less']
})
export class CommentsViewComponent extends UserAware implements OnInit, OnDestroy {

  /**
   * View child for dummy first comment.
   */
  @ViewChild('dummyInitialComponent') dummyInitialComponent: ElementRef;

  /**
   * Configured datastore delegate.
   */
  @Input() set commentsPagingDelegate(delegate: PagedDatastoreDelegate<Comment>) {
    if (delegate) {
      this._commentsPagingDelegate = delegate;
      this._commentsPagingDelegate.loadResults(null, true);
      this._commentsPagingDelegate.removeItemCallback = (commentId) => this.commentService.deleteComment(commentId);
      this._commentsPagingDelegate.refreshResultsCallback = (ids, opts) => (
        this.commentService.findCommentsByIds(ids, opts)
      );
    }
  }

  /**
   * Adjacent getter for the comments paging delegte.
   */
  get commentsPagingDelegate(): PagedDatastoreDelegate<Comment> {
    return this._commentsPagingDelegate;
  }

  /**
   * Private ref of comments paging delegate.
   */
  private _commentsPagingDelegate: PagedDatastoreDelegate<Comment>;

  constructor(
    authService: AuthService,
    private commentService: CommentService
  ) {
    super(authService);
    this.initCallback(() => {});
  }

  /**
   * Remove a comment.
   *
   * @param commentId id of comment to remove.
   */
  public removeComment(commentId: number): void {
    this.commentsPagingDelegate.removeItemFromDatastore(commentId);
  }

  /**
   * Add a comment.
   *
   * @param comment comment to add.
   */
  public addComment(comment: Comment) {
    this.commentsPagingDelegate.addItemToDatastore(comment)
    .then(() => {
      // Scroll to the dummy comment div.
      this.dummyInitialComponent.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'nearest' });
    });
  }

  /**
   * Up or down vote the comment.
   *
   * @param comment comment to vote on.
   * @param up indicates up or down vote action.
   */
  public voteOnComment(comment: Comment, up: boolean): Promise<any> {
    // We have the response.
    return this.commentService.voteOnComment(comment.id, up)
    .then(() => (this.commentsPagingDelegate.refreshPageOfItem(comment.id)));
  }
}
