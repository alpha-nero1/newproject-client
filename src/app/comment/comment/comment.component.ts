/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Utils } from 'src/app/utils/utils';
import { User } from '../../auth/user.model';
import { PagedDatastoreDelegate } from '../../common/paged-datastore-delegate/paged-datastore-delegate';
import { Comment } from '../types/comment.model';
import { CommentService } from '../comment.service';
import { Subscription } from 'rxjs';
import { SortValue } from '../../shared/types/filter.interface';

/**
 * @author Alessandro Alberga
 * @description Comment component.
 */
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.less']
})
export class CommentComponent implements OnInit {

  /**
   * Current user object.
   */
  @Input() public user: User;

  /**
   * Setter of comments.
   */
  @Input() public set comment(comment: Comment) {
    if (comment) {
      this.processNewComment(comment);
    }
  }

  /**
   * Getter for comments.
   */
  public get comment(): Comment {
    return this._comment;
  }

  /**
   * Emit this comments id for deletion.
   */
  @Output() public removeComment = new EventEmitter<number>();

  /**
   * Vote on comment emitter.
   */
  @Output() public votedOnComment = new EventEmitter<boolean>();

  /**
   * Private comment ref.
   */
  private _comment: Comment;

  /**
   * Index for managing replies (which has been selected.)
   */
  public replyIndex: number;

  /**
   * Local reference of date function from utils to use in template.
   */
  public transformToLocaleDate = Utils.TransformToLocaleDate;

  /**
   * Get time since date function.
   */
  public GetTimeSinceDate = Utils.GetTimeSinceDate;

  /**
   * Comment paging dlegate for the comment replies.
   */
  public commentRepliesPagingDelegate: PagedDatastoreDelegate<Comment>;

  /**
   * Active comment sub.
   */
  private activeCommentSub: Subscription;

  /**
   * Actual active comment.
   */
  public activeComment = { activeCommentId: null };

  /**
   * @param sitesService service to get site info.
   */
  constructor(
    private commentService: CommentService
  ) { }

  ngOnInit(): void {
    this.activeCommentSub = this.commentService.activeComment.subscribe(activeComment => {
      this.activeComment = activeComment;
    });
  }

  /**
   * Accept and store a new comment object.
   *
   * @param comment new comment object.
   */
  private processNewComment(comment: Comment): void {
    // Load the new vars.
    this._comment = {
      ...comment,
      // Keep old replies if new ones did not come in.
      replies: (comment.replies || this.comment?.replies),
      created_at: Utils.GetTimeSinceDate(comment.created_at),
      userOwnsComment: (comment?.user_id === this.user?.id),
      vote_count: (comment?.upvotes?.count - comment?.downvotes?.count)
    };
    if (!this.commentRepliesPagingDelegate) {
      // If we did not already instantiate this then lets do that.
      // This ensures we always persist replies.
      this.commentRepliesPagingDelegate = new PagedDatastoreDelegate<Comment>({
        removeItemCallback: (commentId) => this.commentService.deleteComment(commentId),
        addItemCallback: (commentToAdd) => this.commentService.replyToComment(this.comment.id, commentToAdd),
        loadResultsCallback: (opts) => this.commentService.getCommentReplies(this.comment.id, opts),
        refreshResultsCallback: (ids, opts) => this.commentService.findCommentsByIds(ids, opts)
      });
      this.commentRepliesPagingDelegate.queryOpts.sort_by_value = SortValue.Ascending;
      if (this.comment?.replies?.rows) {
        // Set the replies if we have them.
        this.commentRepliesPagingDelegate.upsertDataStore(
          0,
          this._comment?.replies?.rows,
          this._comment?.replies?.count
        );
      }
    }
  }

  /**
   * Set this comment as active.
   */
  public setCommentAsActive(): void {
    this.commentService.setActiveComment(this.comment?.id);
  }

  /**
   * Reply to this comment.
   *
   * @param comment comment to create.
   */
  public addReplyToComment(comment: Comment): void {
    this.commentRepliesPagingDelegate.addItemToDatastore({
      ...comment,
      parent_comment_id: this.comment.id
    });
  }

  /**
   * Remove a reply.
   *
   * @param commentId reply comment id to delete.
   */
  public removeReplyToComment(commentId: number): void {
    this.commentRepliesPagingDelegate.removeItemFromDatastore(commentId);
  }

  /**
   * Emit that the delete on this comment was clicked.
   */
  public removeThisComment(): void {
    this.removeComment.emit(this.comment?.id);
  }

  /**
   * Vote on this comment.
   *
   * @param up up event.
   */
  public voteOnThisComment(up: boolean): void {
    this.votedOnComment.emit(up);
  }

  /**
   * Up or down vote the comment.
   *
   * @param upvote indicates up or down vote.
   * @param identifier indicates thew object type we are voting upon.
   */
  public voteOnReplyingComment(commentObj: Comment, up: boolean): Promise<any> {
    // We have the response.
    return this.commentService.voteOnComment(commentObj.id, up)
    .then(() => (this.commentRepliesPagingDelegate.refreshPageOfItem(commentObj.id)));
  }
}
