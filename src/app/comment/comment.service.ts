/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CommentGql, Comment } from './types/comment.model';
import PromiseProvider from '../utils/promiseProvider';
import { GraphQLService } from '../utils/graphQL.service';
import { GeneralQueryOpts } from '../shared/types/filter.interface';
import { RowsAndCount } from '../common/types/rows-and-count.interface';
import { VoteGql } from '../shared/vote/types/vote.interface';

/**
 * @author Alessandro Alberga
 * @description Comment service.
 */
@Injectable()
export class CommentService extends PromiseProvider {

  /**
   * Active comment behaviour subject.
   */
  public activeComment = new BehaviorSubject<{ activeCommentId: number }>({ activeCommentId: null });

  constructor(private gqlService: GraphQLService) {
    super();
  }

  /**
   * Reset what the active comment is.
   */
  public resetActiveComment() {
    this.activeComment.next({ activeCommentId: null });
  }

  /**
   * Set the active comment.
   *
   * @param commentId id to set as active.
   */
  public setActiveComment(commentId: number): void {
    this.activeComment.next({
      activeCommentId: (
        (commentId === this.activeComment.value.activeCommentId) ?
        null :
        commentId
      )
    });
  }

  /**
   * Find comments by ids, usually to refresh a page, note that count will always come back
   * null, we never even queried for it.
   *
   * @param ids ids to get comments of.
   * @param opts general query opts.
   */
  public findCommentsByIds(ids: number[], opts?: GeneralQueryOpts): Promise<Comment[]> {
    const query = `query ($ids: [Int], $query_opts: GeneralQueryOpts) {
      findCommentsByIds(ids: $ids, query_opts: $query_opts) {
        ${CommentGql}
      }
    }`;
    return this.queryToPromise(this.gqlService.runQuery(query, {
      ids,
      query_opts: opts
    }))
    .then(res => res.findCommentsByIds);
  }

  /**
   * Comment on a site.
   *
   * @param parentCommentId id of comment to reply to.
   * @param comment comment object.
   */
  public replyToComment(parentCommentId: number, comment: Comment): Promise<Comment> {
    const mutation = `mutation ($parent_comment_id: Int!, $comment: CommentInput!) {
      replyToComment(parent_comment_id: $parent_comment_id, comment: $comment) {
        ${CommentGql}
      }
    }`;
    return this.queryToPromise(this.gqlService.runMutation(mutation, {
      comment,
      parent_comment_id: parentCommentId
    }))
    .then(res => res.replyToComment);
  }

  /**
   * Reach api to delete a comment from the site.
   *
   * @param commentId id of comment to delte.
   */
  public async deleteComment(commentId: number): Promise<Comment[]> {
    const mutation = ` mutation {
      deleteComment(comment_id: ${commentId}) {
        ${CommentGql}
      }
    }`;
    const res = await this.queryToPromise(this.gqlService.runMutation(mutation, {}));
    return res.deleteComment;
  }

  /**
   * Get replies of a particular comment.
   *
   * @param commentId id of comment to query for replies.
   * @param queryOpts general query options.
   */
  public getCommentReplies(commentId: number, queryOpts: GeneralQueryOpts): Promise<RowsAndCount<Comment>> {
    const query = `query ($comment_id: Int!, $query_opts: GeneralQueryOpts) {
      findRepliesByComment(comment_id: $comment_id, query_opts: $query_opts) {
        count
        rows {
          ${CommentGql}
        }
      }
    }`;
    return this.queryToPromise(this.gqlService.runQuery(query, {
      comment_id: commentId,
      query_opts: queryOpts
    }))
    .then(res => res.findRepliesByComment);
  }

  /**
   * Vote on a comment. return voting results.
   *
   * @param siteCommentId id of comment to vote on.
   * @param up wether up or downvote is happening.
   */
  public async voteOnComment(siteCommentId: number, up: boolean): Promise<any> {
    const mutation = `mutation {
      voteOnComment(comment_id: ${siteCommentId}, up: ${up}) {
       ${VoteGql}
      }
    }`;
    const res = await this.queryToPromise(this.gqlService.runMutation(mutation, {}));
    return res.voteOnComment;
  }
}
