/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { User } from '../../auth/user.model';
import { VoteResponse } from '../../common/voteResponse.model';
import { RowsAndCount } from '../../common/types/rows-and-count.interface';
import { Vote } from '../../shared/vote/types/vote.interface';

/**
 * @author Alessandro Alberga
 * @description Comment interface.
 */
export interface Comment {
  /**
   * Id of comment.
   */
  id?: number;

  /**
   * Parent comment id.
   */
  parent_comment_id?: number;

  /**
   * Id of user who made comment.
   */
  user_id?: number;

  /**
   * User who made the comment (included model).
   */
  user?: User;

  /**
   * Text of the comment.
   */
  text: string;

  /**
   * Avatar link of user.
   */
  avatar?: string;

  /**
   * Upvote res.
   */
  upvotes?: RowsAndCount<Vote>;

  /**
   * Downvote res.
   */
  downvotes?: RowsAndCount<Vote>;

  /**
   * Consolidated count of up and down votes.
   */
  vote_count?: number;

  /**
   * Date comment was made.
   */
  created_at?: any;

  /**
   * Array of commments on this comment.
   */
  replies?: any;

  /**
   * Calculated on front end to display comment differently.
   */
  userOwnsComment?: boolean;

  /**
   * Flag to say that the comment has been newly created bt you.
   */
  isNew?: boolean;
}

export const CommentGql = `
id
parent_comment_id
text
user_id
user {
  id
  username
  email
  profile_photo
}
replies {
  count
  rows {
    id
    parent_comment_id
    text
    user_id
    user {
      id
      username
      email
      profile_photo
    }
    created_at
    upvotes {
      count
      rows {
        id
        user {
          id
        }
      }
    }
    downvotes {
      count
      rows {
        id
        user {
          id
        }
      }
    }
  }
}
created_at
upvotes {
  count
  rows {
    id
    user {
      id
    }
  }
}
downvotes {
  count
  rows {
    id
    user {
      id
    }
  }
}
`;
