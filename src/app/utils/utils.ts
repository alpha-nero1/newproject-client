/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import * as moment from 'moment';
import { Injectable } from '@angular/core';

/**
 * @author Alessandro Alberga
 * @description collection of accessible static utility fuinctions.
 */
@Injectable()
export class Utils {

  /**
   * Return a formatted string of how lonmg ago a date was.
   * @param pastDate date.
   */
  public static GetTimeSinceDate(pastDate: Date): string {
    if (pastDate) {
      return moment(new Date(pastDate), 'YYYY.MM.DD').fromNow();
    }
    return '';
  }

  /**
   * Check if an item exists in an array.
   *
   * @param array the array to check.
   * @param value the value to check.
   * @param keyToCheck key to check in value and arr for comparison.
   */
  public static ItemExistsInArray(array: any[], value: any, keyToCheck?: string): boolean {
    let obj = null;
    if (!keyToCheck) {
      // Evaluate as primitive type.
      obj = array.find(elem => elem === value);
    } else {
      obj = array.find(elem => elem[keyToCheck] === value);
    }
    return Boolean(obj);
  }

  /**
   * Check if nested properties of objects exist.
   *
   * @param { any } object any object you wish to process.
   * @param { String[] } keys sequence of keys to check in object
   *   e.g ['customer', 'provider', 'id'].
   */
  public static CanAccess(object: any, keys: string[]) {
    let canAccess = true;
    if (object && keys && keys.length) {
      // Init with the first.
      let reducedValue = object[keys[0]];
      // do the checks.
      keys.forEach((key, index) => {
        // We are about to try access undefined, can access is then falsy.
        if (!reducedValue) { return false; }
        if (index > 0) {
          reducedValue = reducedValue[key];
        }
      });
      // Did we find the prop.
      canAccess = Boolean(reducedValue);
    } else if (!object) {
      canAccess = false;
    }
    return canAccess;
  }

  /**
   * Clean certain keys entirely from an object.
   */
  public static CleanObjectOfProperties(object: any, props: string[], recursive = true): any {
    const cleanedObject = { ...object };
    // Clean the object by looping its keys.
    Object.keys(cleanedObject).forEach(key => {
      if (props.includes(key)) {
        delete cleanedObject[key];
      } else if (typeof cleanedObject[key] === 'object' && recursive) {
        cleanedObject[key] = Utils.CleanObjectOfProperties(cleanedObject[key], props);
      }
    });
    // Return cleaned object.
    return cleanedObject;
  }

  /**
   * Get an object with only specific properties.
   *
   * @param object object to transform into fewer properties.
   * @param props props to get from object.
   */
  public static GetObjectWithSpecificProps(object: any, props: string[]): any {
    const returnObject = { };
    props.forEach(prop => {
      if (object && object[prop]) {
        returnObject[prop] = object[prop];
      }
    });
    return returnObject;
  }

  /**
   * Transform date string to something readable.
   */
  public static TransformToLocaleDate(date: string | Date) {
    return new Date(date).toLocaleDateString();
  }

  /**
   * Static function expression to get unique values from an array.
   */
  public static GetUnique(array: any[]) {
    return array.filter((value, index, self) =>  self.indexOf(value) === index);
  }

  /**
   * Re-usable function to check if a user is subscribed.
   *
   * @param userId id of user to check subscribed To.
   * @param subscriptionsArr array of subscriptions of a user.
   */
  public static AmISubscribed(userId: number, subscriptionsArr: any[]) {
    const subscribeeIds = subscriptionsArr.map(sub => sub.subscribee_id);
    return subscribeeIds.includes(userId);
  }

  /**
   * Check if two dates are the same.
   *
   * @param dateOne first date.
   * @param dateTwo second date.
   */
  public static TwoDatesAreSame(dateOne: Date, dateTwo: Date): boolean {
    return (
      dateOne.getFullYear() === dateTwo.getFullYear() &&
      dateOne.getMonth() === dateTwo.getMonth() &&
      dateOne.getDate() === dateTwo.getDate()
    );
  }

  /**
   * Transforma date into something that considers today. E.g if the
   * date sent in is 7:38 today then the function will return: 'Today 7:38'.
   * Else it will return a normal LLL formatted date.
   *
   * @param date date to transform.
   */
  public static GetTodayConsideredDate(date: Date): string {
    const datePoint = new Date(date);
    if (Utils.TwoDatesAreSame(datePoint, new Date())) {
      return `Today ${moment(datePoint).format('h:mm A')}`;
    }
    return moment(datePoint).format('LLL');
  }

  /**
   * Generic function for getting an item from a list by id.
   */
  public GetObjectFromListById(id: number, list: any[]): any {
    return list.find(item => item.id === id);
  }

  /**
   * Insert a new obejct where its id matches the list. Then return list.
   *
   * @param newObject new object to insert into correct pos.
   * @param list list to insert into and return;
   */
  public InsertNewObjectWhereIdEquals(newObject: any, list: any[]): any[] {
    let listToReturn = [...list];
    listToReturn = listToReturn.map(item => {
      if (item.id === newObject.id) {
        return newObject;
      } else {
        return item;
      }
    });
    return listToReturn;
  }

  /**
   * Will return the consolidated vote count of an objects
   * upvotes and downvotes.
   */
  public CountVotesOfObject(object: any): number {
    // Short circuit with value of 0 if neither array sent in.
    if (!object.downvotes || !object.upvotes) { return 0; }
    // Calculate count.
    return -object.downvotes.length + object.upvotes.length;
  }
}
