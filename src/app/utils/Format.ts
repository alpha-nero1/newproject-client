export const formatTime = time => {
  return new Date(time).toLocaleDateString();
};

/**
 * count votes for a site
 * @param site site to perform action upon.
 */
export const countVotes = (site: any) => {
  return site.upvotes.length - site.downvotes.length;
};

/**
 *  Format the tags of a site to display them.
 * @param site site to perform action upon.
 */
export const formatTags = (site: any) => {
  return site.tags.map(tag => ` ${tag}`);
};
