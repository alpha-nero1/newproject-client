/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Observable } from 'rxjs';
import { LocalStorageTokens } from '../auth/local-storage.tokens';

/**
 * @author Alessandro Alberga
 * @description extensible class that aids in turning queries into
 * promises and displaying messages with the snack bar.
 */
export default class PromiseProvider {

  /**
   * Turn an observable into a promise and treat it accordingly.
   *
   * @param observable observable event i.e a query.
   */
  public queryToPromise(observable: Observable<any>): Promise<any> {
    return new Promise((resolve) => {
      observable.subscribe(({ data }) => resolve(data), (err) => {
        if (err && err.networkError && err.networkError.status) {
          const status = err.networkError.status;
          if (status === 401) {
            // Kick em out!
            if (!window.location.href.includes('auth')) {
              // If we are not already in auth section. Otherwise it will loop.
              localStorage.removeItem(LocalStorageTokens.authToken);
              window.location.href = '/auth/login';
            }
          }
        }
      });
    });
  }
}
