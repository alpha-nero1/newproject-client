/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

/**
 *  Enum defining gql policy types.
 */
export enum GqlFetchPolicy {
  NetworkOnly = 'network-only',
  CacheFirst = 'cache-first',
  NoCache = 'no-cache'
}

/**
 * @author Alessandro Alberga
 * @description service to run gql queries and mutations.
 */
@Injectable()
export class GraphQLService {

  /**
   * General return type for subscriptions.
   */
  public subscriptionType = `
    id
    user_id
    subscribee_id
    created_at
    subscribee {
      id
      username
    }
    subscriber {
      id
      username
    }
  `;

  /**
   * Constructor.
   *
   * @param apollo apollo client for gql query.
   */
  constructor(private apollo: Apollo) { }

  /**
   * Run a graphql query and return an observable on results.
   *
   * @param fetch fetch type.
   * @param query query string we want to run.
   */
  public runQuery(
    query: string,
    variables?: any,
    fetchPolicy: GqlFetchPolicy = GqlFetchPolicy.NoCache
  ): Observable<any> {
    return this.apollo.watchQuery({
      query: gql`${query}`,
      fetchPolicy,
      variables
    }).valueChanges;
  }

  /**
   * Run a graphql mutation and return response.
   *
   * @param data data we want to send to mutation.
   * @param query query of mutation.
   */
  public runMutation(mutation: string, data?: any): Observable<any> {
    return this.apollo.mutate({
      mutation: gql`${mutation}`,
      variables: data
    });
  }
}
