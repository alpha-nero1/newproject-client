import { Injectable } from '@angular/core';
import { EMPTY } from 'rxjs';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { Observable } from 'rxjs/Observable';
import { SitesService } from '../site/sites.service';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    private auth: AuthService,
  ) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // If no token don't include it.
    if (!this.auth.getToken()) { return next.handle(request); }
    // We do have a token.
    request = request.clone({
      setHeaders: {
        Authorization: this.auth.getToken()
      }
    });
    return next.handle(request);
  }
}
