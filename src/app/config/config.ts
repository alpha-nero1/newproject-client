/**
 * @author Alessandro Alberga
 * @description common configurations for our networking stuff.
 */
export const config = {
  awsS3: {
    url: 'https://newproject25.s3-ap-southeast-2.amazonaws.com',
    region: 'ap-southeast-2',
    // Bytes.
    uploadLimit: 2000000
  }
};
