/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account/account.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { GraphQLService } from '../utils/graphQL.service';
import { NavbarModule } from '../navbar/navbar.module';
import { SiteGridModule } from '../shared/site-grid/site-grid.module';
import { SubscriptionsGridModule } from '../shared/subscriptions-grid/subscriptions-grid.module';
import { FooterModule } from '../footer/footer.module';
import { UserAvatarModule } from '../shared/user-avatar/user-avatar.module';
import { AccountRoutingModule } from './account-routing.module';
import { MainModuleComponentModule } from '../main-module-component/main-module-component.module';
import { ForgotPasswordModule } from '../shared/forgot-password/forgot-password.module';
import { DialogModule } from '../dialog/dialog.module';
import { ImagePickerModule } from '../shared/image-picker/image-picker.module';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatNativeDateModule } from '@angular/material/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatListModule } from '@angular/material/list';
import { UserStatsViewComponent } from './user-stats-view/user-stats-view.component';
import { DisplayNumberPipeModule } from '../common/pipes/number-to-string/display-number.module';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltip, MatTooltipModule } from '@angular/material/tooltip';

/**
 * @author Alessandro Alberga
 * @description Account module.
 */
@NgModule({
  declarations: [
    AccountComponent,
    UserStatsViewComponent
  ],
  imports: [
    AccountRoutingModule,
    CommonModule,
    DialogModule,
    DisplayNumberPipeModule,
    FlexLayoutModule,
    FooterModule,
    ForgotPasswordModule,
    FormsModule,
    ImagePickerModule,
    MainModuleComponentModule,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDividerModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatListModule,
    MatSnackBarModule,
    NavbarModule,
    SiteGridModule,
    SubscriptionsGridModule,
    UserAvatarModule
  ],
  providers: [
    GraphQLService
  ]
})
export class AccountModule { }
