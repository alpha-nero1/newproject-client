/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserStatsViewComponent } from './user-stats-view.component';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AccountRoutingModule } from '../account-routing.module';
import { DialogModule } from '../../dialog/dialog.module';
import { DisplayNumberPipeModule } from '../../common/pipes/number-to-string/display-number.module';
import { FooterModule } from '../../footer/footer.module';
import { ForgotPasswordModule } from '../../shared/forgot-password/forgot-password.module';
import { FormsModule } from '@angular/forms';
import { ImagePickerModule } from '../../shared/image-picker/image-picker.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MainModuleComponentModule } from '../../main-module-component/main-module-component.module';
import { UserAvatarModule } from '../../shared/user-avatar/user-avatar.module';
import { SubscriptionsGridModule } from '../../shared/subscriptions-grid/subscriptions-grid.module';
import { SiteGridModule } from '../../shared/site-grid/site-grid.module';
import { NavbarModule } from '../../navbar/navbar.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatListModule } from '@angular/material/list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatDatepickerModule } from '@angular/material/datepicker';

describe('UserStatsViewComponent', () => {
  let component: UserStatsViewComponent;
  let fixture: ComponentFixture<UserStatsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserStatsViewComponent ],
      imports: [
        AccountRoutingModule,
        CommonModule,
        DialogModule,
        DisplayNumberPipeModule,
        FlexLayoutModule,
        FooterModule,
        ForgotPasswordModule,
        FormsModule,
        ImagePickerModule,
        MainModuleComponentModule,
        MatButtonModule,
        MatCardModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDividerModule,
        MatIconModule,
        MatInputModule,
        MatNativeDateModule,
        MatProgressSpinnerModule,
        MatSlideToggleModule,
        MatListModule,
        MatSnackBarModule,
        NavbarModule,
        SiteGridModule,
        SubscriptionsGridModule,
        UserAvatarModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserStatsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
