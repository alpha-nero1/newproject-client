/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, Input } from '@angular/core';
import { UserStats } from '../types/user-stats.interface';

/**
 * @author Alessandro Alberga
 * @description User stats dum component.
 */
@Component({
  selector: 'app-user-stats-view',
  templateUrl: './user-stats-view.component.html',
  styleUrls: ['./user-stats-view.component.less']
})
export class UserStatsViewComponent {

  /**
   * User stats input.
   */
  @Input() userStats: UserStats;
}
