/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { MainModuleComponent } from '../main-module-component/main-module/main-module.component';
import { AccountComponent } from './account/account.component';

const routes: Routes = [
  {
    path: ':username',
    component: MainModuleComponent,
    children: [
      { path: '', component: AccountComponent }
    ]
  }
];

/**
 * @author Alessandro Alberga
 * @description account routing module.
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {}
