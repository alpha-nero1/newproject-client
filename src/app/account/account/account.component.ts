/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { COMMA, ENTER, BACKSPACE } from '@angular/cdk/keycodes';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from '../../auth/user.model';
import { ImagePickerComponent } from 'src/app/shared/image-picker/image-picker/image-picker.component';
import { DialogService } from 'src/app/dialog/dialog.service';
import { ForgotPasswordComponent } from '../../shared/forgot-password/forgot-password.component';
import { UserAware } from '../../common/user-aware.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatChipInputEvent } from '@angular/material/chips';
import { MenuSelectionsDelegate } from '../../common/menu-selections-delegate/menu-selections-delegate';
import { AccountService } from '../account.service';
import { PagedDatastoreDelegate } from '../../common/paged-datastore-delegate/paged-datastore-delegate';
import { Site } from '../../site/site.model';

/**
 * @author Alessandro Alberga
 * @description Account module.
 */
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.less']
})
export class AccountComponent extends UserAware implements OnInit, OnDestroy {
  /**
   * Subscription to url parameters.
   */
  private paramSubscription: Subscription;

  /**
   * This is the username that the page loaded with and is
   * independant to the user object.
   */
  public username = '';

  /**
   * Reference to user object.
   */
  public viewingUser: User;

  /**
   * Toggle for viewing/editing an account.
   */
  public isActiveUser: boolean;

  /**
   * Seperator keys for mat-chips input.
   */
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, BACKSPACE];

  /**
   * Flag to toggle upload spinner.
   */
  public updatingInformation = false;

  /**
   * Account menu.
   */
  public accountMenu = new MenuSelectionsDelegate({
    menuSelections: [
      { position: 0, name: 'Overview', icon: 'insert_chart_outlined' },
      { position: 1, name: 'General settings', icon: 'person_outline' },
      { position: 2, name: 'Tags', icon: 'label_important' },
      { position: 3, name: 'Subscriptions', icon: 'dynamic_feed' },
      { position: 4, name: 'Security', icon: 'security' }
    ],
    activeMenuSelection: {
      position: 0,
      name: 'Overview',
      icon: 'insert_chart_outlined'
    }
  });

  /**
   * Lets us toggle the add a banner text.
   */
  public userHasNoBanner = false;

  /**
   * Lets us toggle to show image tag or not for viewing user.
   */
  public viewingUserHasNoBanner = false;

  /**
   * Users sites delegate.
   */
  public usersSitesDelegate = new PagedDatastoreDelegate<Site>({
    queryOpts: {
      page: 0,
      page_size: 100
    },
    loadResultsCallback: (opts) => this.accountService.getUsersSites(
      this.username,
      opts
    )
  });

  constructor(
    private activeRoute: ActivatedRoute,
    public authService: AuthService,
    private dialogService: DialogService,
    private snack: MatSnackBar,
    private accountService: AccountService
  ) {
    super(authService);
    this.initCallback(this.userChangedHandler);
  }

  ngOnInit(): void { }

  /**
   * Function to execute when we receive the current user object.
   *
   * @param user user returned by user aware.
   */
  private userChangedHandler = (user: User) => {
    // Subscribe to the url params to know what account to view.
    this.paramSubscription = this.activeRoute.params.subscribe(params => {
      this.username = (params && params.username ? params.username : '');
      this.usersSitesDelegate.reset();
      this.usersSitesDelegate.loadResults(0);
      this.loadUser(this.username)
        .then(() => this.isActiveUser = (user && user.username === this.viewingUser.username));
    });
  }

  /**
   * Get all information back.
   */
  private refreshInformation() {
    this.loadUser(this.user.username);
  }

  /**
   * Load the user using the username paramter.
   */
  private async loadUser(username: string): Promise<void> {
    this.updatingInformation = true;
    this.viewingUser = await this.authService.queryUserByUsername(username);
    this.username = this.viewingUser.username;
    this.viewingUserHasNoBanner = !this.viewingUser.banner_photo;
    return this.accountService.getUserStats(this.username)
    .then(userStats => {
      this.viewingUser.user_stats = userStats;
      this.updatingInformation = false;
    })
    .catch((err) => {
      this.updatingInformation = false;
      throw err;
    });
  }

  /**
   * Add chip to tags.
   *
   * @param event mat chip event containing tag.
   */
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add tag.
    if ((value || '').trim()) {
      this.user.tags.push(value.trim());
    }
    // Reset the input value.
    if (input) {
      input.value = '';
    }
  }

  /**
   * Remove a tag from the tag list.
   *
   * @param tag tag to remove.
   */
  remove(tag: string): void {
    const index = this.user.tags.indexOf(tag);

    if (index >= 0) {
      this.user.tags.splice(index, 1);
    }
  }

  /**
   * Update the personal info section of account component.
   */
  public onUpdatePersonalInfo(): void {
    const userToSave: Partial<User> = {
      username: this.user.username,
      first_name: this.user.first_name,
      last_name: this.user.last_name,
      email: this.user.email,
      date_of_birth: new Date(this.user.date_of_birth)
    };
    this.authService.updateUser(userToSave)
      .then(updatedUser => {
        this.snack.open('User updated', 'Ok', { duration: 4000 });
        // We do this check because the whole ecosystem of data for this compoennt is derived
        // from the user's username and thus its healthier just to refresh.
        if (this.username !== updatedUser.username) {
          window.location.href = `/accounts/${updatedUser.username}/`;
        }
      });
  }

  /**
   * Method triggered on tags form submission.
   */
  public async onUpdateTags(): Promise<void> {
    const { tags } = this.user;
    await this.authService.updateUser({ tags });
    this.snack.open('User tags updated', 'Ok', { duration: 3000 });
  }

  /**
   * Banner clicked handler.
   */
  public bannerClickedHandler(): any {
    // Back out if someone else tries to upload.
    if (!this.isActiveUser) { return; }
    this.dialogService.openFormDialog(
      {
        componentInputs: {
          imageSource: this.user.banner_photo,
          imageDisplayDimensions: {
            width: '70vw',
            height: '30vh'
          }
        },
        formInputs: {
          component: ImagePickerComponent,
          submitText: 'Save',
          title: 'Choose a new banner photo'
        }
      },
      (res: File) => {
        if (res) {
          this.updatingInformation = true;
          return this.authService.uploadUserFile(this.user.id, res, 'banner_photo')
            .then(updatedUser => {
              if (updatedUser) {
                this.snack.open(`Banner updated`, `Ok`, { duration: 3000 });
              }
              this.updatingInformation = false;
              this.refreshInformation();
            })
            .catch(() => this.updatingInformation = false);
        }
      });
  }

  /**
   * Implementation for user avatar click.
   */
  public avatarClickHandler() {
    // Back out if someone else tries to upload.
    if (!this.isActiveUser) { return; }
    this.dialogService.openFormDialog(
      {
        componentInputs: {
          imageSource: this.user.profile_photo,
          imageDisplayDimensions: {
            width: '20vw',
            height: '20vw'
          }
        },
        formInputs: {
          component: ImagePickerComponent,
          submitText: 'Save',
          title: 'Choose a new profile photo'
        }
      },
      (res: File) => {
        if (res) {
          this.updatingInformation = true;
          this.authService.uploadUserFile(this.user.id, res, 'profile_photo')
            .then(updatedUser => {
              if (updatedUser) {
                this.snack.open(`Profile updated`, `Ok`, { duration: 3000 });
              }
              this.updatingInformation = false;
              this.refreshInformation();
            })
            .catch(() => this.updatingInformation = false);
        }
      });
  }

  /**
   * Open forgot password component.
   *
   * @param handle handle to reset password for.
   */
  public openForgotPassword(handle: string): void {
    this.dialogService.openFormDialog({
      formInputs: {
        component: ForgotPasswordComponent,
        title: 'Reset my password',
        icon: 'security'
      },
      componentInputs: {
        handle,
      }
    }, (resetHandle: string) => {
      if (resetHandle) {
        this.authService.initiatePasswordReset(resetHandle);
      }
    },
    null,
    { width: '500px' });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
