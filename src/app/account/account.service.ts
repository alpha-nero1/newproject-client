/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import PromiseProvider from '../utils/promiseProvider';
import { Injectable } from '@angular/core';
import { GraphQLService } from '../utils/graphQL.service';
import { UserStats } from './types/user-stats.interface';
import { RowsAndCount } from '../common/types/rows-and-count.interface';
import { Site, SiteGqlType } from '../site/site.model';
import { GeneralQueryOpts } from '../shared/types/filter.interface';

/**
 * @author Alessandro Alberga
 * @description Angular service for all account management functionality.
 */
@Injectable({
  providedIn: 'root'
})
export class AccountService extends PromiseProvider {

  constructor(
    private qglService: GraphQLService,
  ) {
    super();
  }

  /**
   * Get stats of a particular user.
   *
   * @param userId user id.
   */
  public getUserStats(username: string): Promise<UserStats> {
    const query = `query ($username: String!) {
      getUserStats(username: $username) {
        total_sites_created
        total_comments_made
        total_site_upvotes
        total_site_downvotes
        total_subscribers
        total_subscribed_to
      }
    }`;
    return this.queryToPromise(this.qglService.runQuery(query, {
      username
    }))
      .then(res => res.getUserStats);
  }

  /**
   * Get users subscriptions.
   */
  public getMySubscriptions(): Promise<any> {
    const query = `{
      getMySubscribees {
        count
        rows {
          ${this.qglService.subscriptionType}
        }
      }
    }`;
    return this.queryToPromise(this.qglService.runQuery(query))
      .then(res => res.getMySubscribees);
  }

  /**
   * Get a users subscribers.
   */
  public getMySubscribers(): Promise<any> {
    const query = `{
      getMySubscribers {
        count
        rows {
          ${this.qglService.subscriptionType}
        }
      }
    }`;
    return this.queryToPromise(this.qglService.runQuery(query))
      .then(res => res.getMySubscribers);
  }

  /**
   * Subscribe to a user.
   */
  public subscribe(subscribeeId: number): Promise<any> {
    const mutation = `mutation {
      subscribe(subscribee_id: ${subscribeeId}) {
        ${this.qglService.subscriptionType}
      }
    }`;
    return this.queryToPromise(this.qglService.runMutation(mutation, {}))
      .then(res => res.subscribe);
  }

  /**
   * Unsubscribe to a user.
   */
  public unsubscribe(subscribeeId: number): Promise<any> {
    const mutation = `mutation {
      unsubscribe(subscribee_id: ${subscribeeId}) {
        ${this.qglService.subscriptionType}
      }
    }`;
    return this.queryToPromise(this.qglService.runMutation(mutation, {}))
      .then(res => res.subscribe);
  }

  /**
   * Get users sites.
   *
   * @param userId id of user.
   * @param opts general query opts.
   */
  public getUsersSites(username: string, opts: GeneralQueryOpts): Promise<RowsAndCount<Site>> {
    const gql = `query($username: String!, $query_opts: GeneralQueryOpts) {
      getUsersSites(username: $username, query_opts: $query_opts) {
        count
        rows {
          ${SiteGqlType()}
        }
      }
    }
    `;
    return this.queryToPromise(this.qglService.runQuery(gql, {
      username,
      query_opts: opts
    }))
    .then(res => res.getUsersSites);
  }
}
