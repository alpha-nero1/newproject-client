/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';
import { SitesService } from '../site/sites.service';
import { User } from '../auth/user.model';
import { UserAware } from '../common/user-aware.component';

/**
 * @author Alessandro Alberga
 * @description Navbar component.
 */
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent extends UserAware {

  /**
   * Seach key.
   */
  public searchKey = '';

  public isAdmin = false;

  constructor(
    authService: AuthService,
    private router: Router,
  ) {
    super(authService);
    this.initCallback(this.saveUser);
  }

  /**
   * Handler for saving the new user object.
   */
  private saveUser = (user) => {
    this.authService.canAccessAdminConsole()
    .then((can: boolean) => {
      this.isAdmin = can;
    });
  }

  /**
   * Log user out.
   */
  public logout(): void {
    this.authService.logout();
  }

  /**
   * Uses the router to send the user to their account page.
   */
  public navigateToAccount(): void {
    this.router.navigate([`/accounts/${this.user.username}`]);
  }

  public navigateToNotifications(): void {
    this.router.navigate([`/notifications`]);
  }

  /**
   * Applies search key and reaches service to perform api call.
   */
  async onSearch(): Promise<void> {
    // if key is empty just redirect to home.
    if (this.searchKey.length > 1) {
      // Filter sites and emit.
      this.router.navigate([`/home/search/${this.searchKey}`]);
    } else {
      this.refreshHome();
    }
  }

  public refreshHome(): void {
    this.router
      .navigateByUrl('/DummyComponent', { skipLocationChange: true })
      .then(() => this.router.navigate(['/home']));
  }

  public navigateHome(): void {
    this.router.navigate(['/home/']);
  }
}
