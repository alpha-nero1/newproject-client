/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component } from '@angular/core';

/**
 * @author Alessandro Alberga
 * @description simple 404 error component.
 */
@Component({
  selector: 'app-fourhundredfour',
  templateUrl: './fourhundredfour.component.html',
  styleUrls: ['./fourhundredfour.component.less']
})
export class FourHundredFourComponent { }
