/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FourHundredFourComponent } from './fourhundredfour/fourhundredfour.component';
import { ErrorsRoutingModule } from './errors-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

/**
 * @author Alessandro Alberga
 * @description Module handling our error pages.
 */
@NgModule({
  declarations: [FourHundredFourComponent],
  imports: [
    CommonModule,
    ErrorsRoutingModule,
    MatIconModule,
    FlexLayoutModule,
    MatButtonModule
  ]
})
export class ErrorsModule { }
