/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationCancel, NavigationEnd, NavigationError, RouterEvent } from '@angular/router';
import { AuthService } from './auth/auth.service';

/**
 * @author Alessandro Alberga
 * @description Applications main AppComponent class.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  /**
   * Navigation is loading toggle.
   */
  public navigationLoading: boolean;

  constructor(
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.router.events.subscribe(this.routeHandler);
    this.authService.getMe();
    const preventDefaultFileOpen = event => {
      if (event && event.preventDefault) {
        event.preventDefault();
      }
    };
    window.addEventListener('dragover', preventDefaultFileOpen, false);
    window.addEventListener('drop', preventDefaultFileOpen, false);
  }

  /**
   * Route navigation subscription handler.
   */
  private routeHandler = (event: RouterEvent) => {
    switch (true) {
      case event instanceof NavigationStart: {
        this.navigationLoading = true;
        break;
      }
      case event instanceof NavigationEnd:
      case event instanceof NavigationCancel:
      case event instanceof NavigationError: {
        this.navigationLoading = false;
        break;
      }
    }
  }
}
