/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit } from '@angular/core';
import { Site } from 'src/app/site/site.model';
import { SitesService } from 'src/app/site/sites.service';
import { PagedDatastoreDelegate } from '../../common/paged-datastore-delegate/paged-datastore-delegate';

/**
 * @author Alessandro Alberga
 * @description Home component.
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  /**
   * Value of slider toggle on or off.
   */
  public checked = true;

  /**
   * Trending sites delegate.
   */
  public trendingSitesDelegate = new PagedDatastoreDelegate<Site>({
    queryOpts: {
      page: 0,
      page_size: 100
    },
    loadResultsCallback: (opts) => this.siteService.getTrendingSites(opts)
  });

  /**
   * User's recommended sites delegate.
   */
  public recommendedSitesDelegate = new PagedDatastoreDelegate<Site>({
    queryOpts: {
      page: 0,
      page_size: 100
    },
    loadResultsCallback: (opts) => this.siteService.getRecommendedSites(opts)
  });

  constructor(private siteService: SitesService) { }

  ngOnInit(): void {
    // Load site information.
    this.recommendedSitesDelegate.loadResults(0);
    this.trendingSitesDelegate.loadResults(0);
  }

  /**
   * Save checked toggle in home service. (Relevant for seaching).
   */
  public toggleChecked(): void {
    this.checked = !this.checked;
  }
}
