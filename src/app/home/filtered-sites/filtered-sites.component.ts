/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Site } from '../../site/site.model';
import { countVotes } from '../../utils/Format';
import { Router, ActivatedRoute } from '@angular/router';
import { SitesService } from 'src/app/site/sites.service';
import { PagedDatastoreDelegate } from '../../common/paged-datastore-delegate/paged-datastore-delegate';

/**
 * @author Alessandro Albega
 * @description FilteredSitesCompnent, shows the results of site search.
 */
@Component({
  selector: 'app-filtered-sites',
  templateUrl: './filtered-sites.component.html',
  styleUrls: ['./filtered-sites.component.css']
})
export class FilteredSitesComponent implements OnInit, OnDestroy {

  /**
   * Filtered sites delegate.
   */
  public filteredSitesDelegate = new PagedDatastoreDelegate<Site>({
    queryOpts: {
      page: 0,
      page_size: 100
    },
    loadResultsCallback: opts => this.siteService.searchSites(
      this.searchKey,
      [],
      opts
    )
  });

  /**
   * Search key that produced results.
   */
  public searchKey: string;

  /**
   * Simple subscription to param args.
   */
  private paramsSubscription: Subscription;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private siteService: SitesService
  ) { }

  ngOnInit(): void {
    this.paramsSubscription = this.activeRoute.params.subscribe(params =>
      this.searchHandler(params));
  }

  /**
   * Handler for acting upon a search url action.
   */
  private searchHandler(params: any): void {
    this.searchKey = params.searchKey;
    // Filter sites and save them.
    this.filteredSitesDelegate.reset();
    this.filteredSitesDelegate.loadResults(0);
  }

  /**
   * Simple navigate home function for the home button when no
   * results are displayed.
   */
  public navigateHome(): void {
    // Clear the key
    this.searchKey = '';
    this.router.navigate(['/home/']);
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }
}
