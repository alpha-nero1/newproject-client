/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { AdminService } from '../../admin/admin.service';
import { GeneralQueryOpts } from '../../shared/types/filter.interface';
import { DailyFeature } from '../../admin/types/daily-feature.interface';
import { SitesService } from '../../site/sites.service';
import { Router } from '@angular/router';
import * as moment from 'moment';

/**
 * @author Alessandro Alberga
 * @description Daily feature component.
 */
@Component({
  selector: 'app-dailyfeature',
  templateUrl: './dailyfeature.component.html',
  styleUrls: ['./dailyfeature.component.less']
})
export class DailyfeatureComponent implements OnInit {

  private queryOpts: GeneralQueryOpts = {
    page: 0,
    page_size: 1
  };

  public dailyFeature: DailyFeature;

  constructor(private siteService: SitesService, private router: Router) { }

  ngOnInit(): void {
    this.siteService.getDailyFeatures(this.queryOpts)
    .then(({ rows }) => {
      this.dailyFeature = rows[0];
    });
  }

  public getDate() {
    const newDate = new Date(this.dailyFeature?.created_at);
    return moment(newDate).format('LL');
  }

  public buttonClick(): void {
    this.router.navigate([`/sites/${this.dailyFeature?.site?.uuid}`]);
  }
}
