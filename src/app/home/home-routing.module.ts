/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { MainModuleComponent } from '../main-module-component/main-module/main-module.component';
import { HomeComponent } from './home/home.component';
import { FilteredSitesComponent } from './filtered-sites/filtered-sites.component';

const routes: Routes = [
  {
    path: '',
    component: MainModuleComponent,
    children: [
      { path: '', pathMatch: 'full', component: HomeComponent },
      { path: 'search/:searchKey', component: FilteredSitesComponent }
    ]
  }
];

/**
 * @author Alessandro Alberga
 * @description home routing module.
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
