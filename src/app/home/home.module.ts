/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { DailyfeatureComponent } from './dailyfeature/dailyfeature.component';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FilteredSitesComponent } from './filtered-sites/filtered-sites.component';
import { NavbarModule } from '../navbar/navbar.module';
import { SiteGridModule } from '../shared/site-grid/site-grid.module';
import { FooterModule } from '../footer/footer.module';
import { HomeRoutingModule } from './home-routing.module';
import { MainModuleComponentModule } from '../main-module-component/main-module-component.module';
import { AdModule } from '../shared/ad/ad.module';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatExpansionModule } from '@angular/material/expansion';

/**
 * @author Alessandro Alberga
 * @description Home module.
 */
@NgModule({
  declarations: [
    HomeComponent,
    DailyfeatureComponent,
    FilteredSitesComponent
  ],
  imports: [
    AdModule,
    CommonModule,
    FlexLayoutModule,
    FooterModule,
    FormsModule,
    HomeRoutingModule,
    MainModuleComponentModule,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatExpansionModule,
    MatIconModule,
    MatSlideToggleModule,
    MatGridListModule,
    NavbarModule,
    SiteGridModule
  ],
  bootstrap: [HomeComponent]
})
export class HomeModule { }
