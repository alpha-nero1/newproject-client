/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { User } from '../auth/user.model';
import { Site } from '../site/site.model';

/**
 * @author Alessandro Alberga
 * @description interface for home page daily feature.
 */
export interface DailyFeature {
  /**
   * What is said under the title.
   */
  subtext: string;

  /**
   * Banner image link.
   */
  bannerImage: string;

  /**
   * User associated with the daily feature.
   */
  user: User;

  /**
   * Site that is featured.
   */
  site: Site;
}
