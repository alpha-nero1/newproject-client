/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

/**
 * @author Alessandro Alberga
 * @description Configurable table object.
 */
export class NPTableObject<T> {

  /**
   * Loading flag.
   */
  loading?: boolean;

  /**
   * If table is clickable.
   */
  clickable?: boolean;

  /**
   * Columns.
   */
  columns: NPTableColumn[] = [];

  /**
   * Table data.
   */
  dataSource = new MatTableDataSource<T>();

  /**
   * Table page.
   */
  page = 0;

  /**
   * Count of total rows.
   */
  count = 0;

  /**
   * Mat paginator.
   */
  public paginator: MatPaginator;

  /**
   * Page size.
   */
  public pageSize = 20;

  /**
   * Potential callback to be fired on page change.
   */
  pageChangeCallback = (page) => {};

  constructor(opts?: Partial<NPTableObject<T>>) {
    if (opts) {
      this.dataSource = opts.dataSource || this.dataSource;
      this.page = opts.page;
      this.count = opts.count;
      this.paginator = opts.paginator;
      this.pageChangeCallback = opts.pageChangeCallback;
      this.columns = opts.columns;
    }
  }

  /**
   * Get the data.
   */
  public getData(): T[] {
    return [...this.dataSource.data];
  }

  /**
   * Get the data.
   */
  public getDataSource(): MatTableDataSource<T> {
    return this.dataSource;
  }

  /**
   * Set the data.
   *
   * @param data data to set.
   */
  public setData(data: T[]): void {
    this.dataSource.data = data;
  }

  /**
   * Get the page.
   */
  public getPage(): number {
    return this.page;
  }

  /**
   * Set the page.
   *
   * @param page new page value.
   */
  public setPageChange(func: (page) => {}): void {
    this.pageChangeCallback = func;
  }

  /**
   * Set the page.
   *
   * @param page new page value.
   */
  public setPage(page: number): void {
    this.page = page;
    this.pageChangeCallback(page);
  }

  /**
   * Get the count.
   */
  public getCount(): number {
    return this.count;
  }

  /**
   * Set the count.
   *
   * @param count new count value.
   */
  public setCount(count: number): void {
    this.count = count;
  }

  /**
   * Get the columns set.
   */
  public getColumns(): NPTableColumn[] {
    return [ ...this.columns];
  }

  /**
   * Set new columns.
   *
   * @param page new page value.
   */
  public setColumns(columns: NPTableColumn[]): void {
    this.columns = columns;
  }

  /**
   * Get the properties that are displayed.
   */
  public getDisplayProps(): string[] {
    return this.columns.map(c => c.property);
  }

  /**
   * Function to set that will execute code on a row click.
   */
  public rowClickHandler = (row) => null;
}

/**
 * Table column.
 */
export interface NPTableColumn {
  /**
   * The property of the data to take for each row.
   */
  property: string;

  /**
   * The header text of the column.
   */
  header: string;
}
