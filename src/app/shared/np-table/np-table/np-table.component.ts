/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { NPTableObject } from '../types/np-table-object.model';
import { PageEvent, MatPaginator } from '@angular/material/paginator';

/**
 * @author Alessandro Alberga
 */
@Component({
  selector: 'app-np-table',
  templateUrl: './np-table.component.html',
  styleUrls: ['./np-table.component.less']
})
export class NpTableComponent implements OnInit {

  /**
   * Table obejct.
   */
  @Input() public tableObject = new NPTableObject<any>();

  /**
   * No data message.
   */
  @Input() public noDataMessage = 'No results.';

  /**
   * Table obejct event.
   */
  @Output() public tableObjectChange = new EventEmitter<NPTableObject<any>>();

  /**
   * Paginator ref.
   */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor() { }

  ngOnInit(): void { }

  /**
   * Get value.
   *
   * @param row row
   * @param prop property to dissect row.
   */
  public getValue(row: any, prop: string) {
    return row[prop];
  }

  /**
   * Set page func.
   *
   * @param event paging event.
   */
  public setPage(event: PageEvent) {
    this.tableObject.setPage(event.pageIndex);
    this.tableObjectChange.next(this.tableObject);
  }
}
