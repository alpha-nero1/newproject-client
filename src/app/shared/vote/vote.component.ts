/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, Output, Input, EventEmitter } from '@angular/core';

/**
 * @author Alessandro Alberga
 * @description component standardising the way votes are
 *   show within the application.
 */
@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
})
export class VoteComponent {

  /**
   * Votes numeral to display.
   */
  @Input() public votes = 0;

  /**
   * Event output for the vote action.
   */
  @Output() public voted = new EventEmitter<boolean>();

  constructor() { }

  /**
   * On vote handler to emit a click from the component.
   *
   * @param on boolean true for upvote, false for downvote
   */
  public onVote(on): void {
    this.voted.emit(on);
  }
}
