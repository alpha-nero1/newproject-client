/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { StandardDatabaseRecord } from '../../../common/types/standard-database-record.interface';

/**
 * @author Alessandro Alberga
 * @description interface describing a vote.
 */
export interface Vote extends StandardDatabaseRecord {
  user_id: number;
  site_id?: number;
  comment_id?: number;
}

export const VoteGql = `
upvotes {
  count
  rows {
    id
    user {
      id
    }
  }
}
downvotes {
  count
  rows {
    id
    user {
      id
    }
  }
}
`;
