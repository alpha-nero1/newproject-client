/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Site } from 'src/app/site/site.model';
import { PagedDatastoreDelegate } from '../../common/paged-datastore-delegate/paged-datastore-delegate';
import { Router } from '@angular/router';
import { Utils } from '../../utils/utils';

/**
 * @author Alessandro Alberga
 * @description simple component that accepts a list of site objects and
 *  displayes them in a standardised way. Auzgezeichnet!
 */
@Component({
  selector: 'app-site-grid',
  templateUrl: './site-grid.component.html',
  styleUrls: ['./site-grid.component.less']
})
export class SiteGridComponent implements OnInit, OnDestroy {

  /**
   * Set the paging delegate.
   */
  @Input() public set sitesPagingDelegate(delegate: PagedDatastoreDelegate<Site>) {
    if (delegate) {
      this._sitesPagingDelegate = delegate;
      console.log(this._sitesPagingDelegate)
    }
  }

  /**
   * Get sites paging delegate.
   */
  public get sitesPagingDelegate(): PagedDatastoreDelegate<Site> {
    return this._sitesPagingDelegate;
  }

  /**
   * Sites paging datastore delegate.
   */
  private _sitesPagingDelegate: PagedDatastoreDelegate<Site>;

  /**
   * Is loading indicator for loading more results.
   */
  public isLoading = false;

  /**
   * Number of columns to use.
   */
  public columnsNum = 4;

  public GetTimeSinceDate = Utils.GetTimeSinceDate;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.evaluateColumnsNumber();
    window.addEventListener('resize', this.evaluateColumnsNumber);
  }

  public goto(site) {
    this.router.navigate([`/sites/${site.uuid}`]);
  }

  /**
   * Evaluate and store how many columns we want to be showing.
   */
  private evaluateColumnsNumber: () => void = () => {
    const divis = (window.innerWidth / 420);
    this.columnsNum = Math.ceil(divis);
  }

  /**
   * Load in new results if needed.
   */
  public loadNewResults(): any {
    if (!this.isLoading) {
      this.isLoading = true;
      this.sitesPagingDelegate?.loadResults()
      .then(() => {
        this.isLoading = false;
      });
    }
  }

  ngOnDestroy(): void {
    window.removeEventListener('resize', this.evaluateColumnsNumber);
  }
}
