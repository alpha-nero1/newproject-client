/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteGridComponent } from './site-grid.component';
import { UserAvatarModule } from '../user-avatar/user-avatar.module';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { DetectInViewModule } from '../../common/directives/detect-in-view/detect-in-view.module';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DisplayNumberPipeModule } from '../../common/pipes/number-to-string/display-number.module';

/**
 * @author Alessandro Alberga
 * @description Site grid component wrapper module.
 */
@NgModule({
  declarations: [SiteGridComponent],
  imports: [
    CommonModule,
    DetectInViewModule,
    DisplayNumberPipeModule,
    MatGridListModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatProgressSpinnerModule,
    FlexLayoutModule,
    RouterModule,
    UserAvatarModule,
    RouterModule
   ],
  exports: [SiteGridComponent],
})
export class SiteGridModule {}
