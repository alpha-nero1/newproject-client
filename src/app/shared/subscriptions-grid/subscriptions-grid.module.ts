/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionsGridComponent } from './subscriptions-grid.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { UserAvatarModule } from '../user-avatar/user-avatar.module';

/**
 * @author Alessandro Alberga
 * @description subscriptions grid component wrapper module.
 */
@NgModule({
  declarations: [SubscriptionsGridComponent],
  imports: [
    CommonModule,
    MatGridListModule,
    UserAvatarModule
  ],
  exports: [SubscriptionsGridComponent]
})
export class SubscriptionsGridModule {}
