/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, Input } from '@angular/core';

/**
 * @author Sanjay Albegra
 * @description Simple component listing subscribers.
 */
@Component({
  selector: 'app-subscriptions-grid',
  templateUrl: './subscriptions-grid.component.html',
  styleUrls: ['./subscriptions-grid.component.css']
})
export class SubscriptionsGridComponent implements OnInit {

  /**
   * Subscriptions input list to display in the component.
   */
  @Input() subscriptions: any[] = [];

  constructor() { }

  ngOnInit(): void { }

  public getSubscriptionUser(sub: any) {
    return (
      sub.subscribee ?
      sub.subscribee :
      sub.subscriber
    );
  }

}
