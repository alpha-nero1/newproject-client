/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, Input, EventEmitter, Output } from '@angular/core';
import { User } from '../../auth/user.model';

/**
 * @author Alessandro Alberga
 * @description simple component for handling user avatars across the app.
 */
@Component({
  selector: 'app-user-avatar',
  templateUrl: './user-avatar.component.html',
  styleUrls: ['./user-avatar.component.less']
})
export class UserAvatarComponent {

  /**
   * Photo url input to display profile photo.
   */
  @Input() public photoUrl: string;

  /**
   * Set the height of the icon.
   */
  @Input() public height = 40;

  /**
   * User.
   */
  @Input() set user(user: User) {
    if (user && user.first_name && user.last_name) {
      const flfn = user.first_name[0];
      const flln = user.last_name[0];
      this.letters = [flfn, flln];
    }
  }

  /**
   * Allow detection of avatar click so components can react to
   * said action.
   */
  @Output() public avatarClicked = new EventEmitter<any>();

  /**
   * Show letter image flag.
   */
  public showLetterImage = false;

  public letters = [];

  constructor() { }

  /**
   * Simply emit that the avatar was clicked.
   */
  public avatarClickHandler(): void {
    this.avatarClicked.emit();
  }

  public imageErrored() {
    this.showLetterImage = true;
  }
}
