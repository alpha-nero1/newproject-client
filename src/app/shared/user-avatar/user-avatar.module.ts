import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserAvatarComponent } from './user-avatar.component';
import { FlexLayoutModule } from '@angular/flex-layout';

/**
 * @auhtor Alessandro Alberga
 * @description wrapper module for showing a user avatar.
 */
@NgModule({
  declarations: [UserAvatarComponent],
  imports: [
    CommonModule,
    FlexLayoutModule
  ],
  exports: [UserAvatarComponent],
  providers: [],
})
export class UserAvatarModule {}
