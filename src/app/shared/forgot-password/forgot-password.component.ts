/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, Input } from '@angular/core';
import { InjectedFormDialog } from '../../dialog/injected-form-dialog.interface';
import { Validator, ValidResponse } from '../../common/validator.service';

/**
 * @author Alessandro Alberga
 * @descrition component to kick off the password reset process.
 */
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit, InjectedFormDialog {

  /**
   * Handle of user to send reset to (email)
   */
  @Input() public handle = '';

  /**
   * Reference to email validity object.
   */
  public emailValidity: ValidResponse;

  constructor() { }

  ngOnInit(): void {
    this.checkEmail();
  }

  /**
   * Check an email on model change.
   */
  public checkEmail(): void {
    this.emailValidity = Validator.validateEmail(this.handle);
  }

  /**
   * Submit handle.
   */
  public submit(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.emailValidity.valid) {
        resolve(this.handle);
      } else {
        reject(this.emailValidity.reasons);
      }
    });
  }
}
