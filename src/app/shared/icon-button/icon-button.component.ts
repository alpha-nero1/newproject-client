/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * @author Alessandro Alberga
 * @description Icon button component (button with awesome custom functionality).
 */
@Component({
  selector: 'app-icon-button',
  templateUrl: './icon-button.component.html',
})
export class IconButtonComponent {

  /**
   * Initial toggle input for the subscribed button.
   */
  @Input() iconOn = false;

  /**
   * Icon to show in on state.
   */
  @Input() onIcon = 'remove';

  /**
   * Set the hover color.
   */
  @Input() hoverColor: string;

  /**
   * Icon to show in off state
   */
  @Input() offIcon = 'add';

  /**
   * Colour to show on on state.
   */
  @Input() onColour = 'red';

  /**
   * Colour to show on off state.
   */
  @Input() offColour = 'blue';

  /**
   * Tooltip to show for button.
   */
  @Input() onTooltip = '';

  @Input() offTooltip = '';

  /**
   * Disable toggle in case owner is viewing user.
   */
  @Input() disabled = false;

  /**
   * Click event emmiter to manage subscription action.
   */
  @Output() iconOnChanged = new EventEmitter<boolean>();

  /**
   * Handle button click, invert the subscribed property and emit
   * what that inversion is.
   */
  public onIconClick(): void {
    if (!this.disabled) {
      this.iconOn = !this.iconOn;
      this.iconOnChanged.emit(this.iconOn);
    }
  }

  /**
   * Simple function getting us the colour required for the button.
   */
  public getIconColour(): string {
    if (this.disabled) {
      return 'lightgray';
    }
    if (this.iconOn) {
      return this.onColour;
    } else {
      return this.offColour;
    }
  }

  /**
   * Get the tooltip to show.
   */
  public getTooltip(): string {
    if (this.iconOn) {
      return this.onTooltip;
    } else {
      return this.offTooltip;
    }
  }
}
