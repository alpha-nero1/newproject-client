/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, ViewChild, Input, OnInit } from '@angular/core';
import { InjectedFormDialog } from 'src/app/dialog/injected-form-dialog.interface';

/**
 * @author Alessandro Alberga
 * @description Image picker component to allow for image retrival from the
 *  user.
 */
@Component({
  selector: 'app-image-picker',
  templateUrl: './image-picker.component.html',
  styleUrls: ['./image-picker.component.less']
})
export class ImagePickerComponent implements InjectedFormDialog {

  /**
   * Data that is initially loaded in via our component factory.
   */
  public data: any;

  /**
   * Uploader input ref.
   */
  @ViewChild('uploaderInput') uploaderInput;

  /**
   * Wether or not to allow editing of the picker.ss
   */
  @Input() allowEdit = true;

  /**
   * Injected image source
   */
  @Input() imageSource;

  @Input() imageHeight = 40;

  /**
   * Allow configuration of display dimensions.
   */
  @Input() imageDisplayDimensions: {
    height: string,
    width: string
  };

  /**
   * Selected image from template.
   */
  public selectedImage: File;

  /**
   * Toggled by template if user is dragging.
   */
  public dragging: boolean;

  /**
   * On file chnaged handler.
   *
   * @param event event for file selection.
   */
  public onFileChanged(file: File): void {
    this.selectedImage = file;
    const fileReader = new FileReader();
    fileReader.onload = e => this.imageSource = fileReader.result;
    fileReader.readAsDataURL(this.selectedImage);
  }

  /**
   * Handle the image drop.
   *
   * @param event onDrop event.
   */
  public onImageDrop(event): void {
    event.preventDefault();
    this.dragging = false;
    const file = event.dataTransfer.files[0];
    this.onFileChanged(file);
  }

  /**
   * Handles the image drag over drop zone.
   *
   * @param event standard drag event.
   */
  public onImageDragOver(event): void {
    event.preventDefault();
    event.stopPropagation();
    event.dataTransfer.dropEffect = 'copy';
    if (!this.dragging) {
      // To prevent chnage detection going off.
      this.dragging = true;
    }
  }

  /**
   * Submit handler.
   */
  public submit(): Promise<File> {
    return new Promise(resolve => {
      resolve(this.selectedImage);
    });
  }
}
