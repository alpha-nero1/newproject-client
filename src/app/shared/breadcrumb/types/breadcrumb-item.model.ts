/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { VariableInstanceLoader } from '../../../common/graphql-helpers/variable-instance-loader';

/**
 * @author Alessandro Alberga
 * @description Class to facilitate breadcrumb items.
 */
export class BreadcrumbItem {

  /**
   * Display properties to format display.
   */
  public displayProperties?: string[];

  /**
   * Potential link to nav to on click.
   */
  public link?: string;

  /**
   * Linking object.
   */
  public linkingObject?: any;

  constructor(breadcrumbConfig: Partial<BreadcrumbItem>) {
    VariableInstanceLoader(this, breadcrumbConfig);
  }

  /**
   * Display handler function.
   */
  public display(): string {
    let displayString = '';
    if (this.linkingObject && this.displayProperties) {
      this.displayProperties.forEach(prop => {
        if (prop && this.linkingObject[prop]) {
          displayString += `${this.linkingObject[prop]}`;
        }
      });
    }
    return displayString;
  }
}
