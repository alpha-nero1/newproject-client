/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, Input } from '@angular/core';
import { BreadcrumbItem } from '../types/breadcrumb-item.model';
import { Router } from '@angular/router';

/**
 * @author Alessandro Alberga
 * @description Component facilitating in app breadcrumbs.
 */
@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.less']
})
export class BreadcrumbComponent {

  /**
   * Input for the breadcrumb chain to show.
   */
  @Input() public breadcrumbChain: BreadcrumbItem[] = [];

  /**
   * The text that seperates each breadcrumb item.
   */
  @Input() public seperatingText = ' < ';

  @Input() public nullDisplayReplacement: string;

  constructor(private router: Router) { }

  public displayBreadcrumb(breadcrumb: BreadcrumbItem): string {
    const display = breadcrumb.display();
    if (display && display.length) {
      return display;
    }
    return this.nullDisplayReplacement;
  }

  /**
   * Breadcrumb clicked handler, will route to link if var is present in item.
   *
   * @param crumb crumb item corresponding to that which was clicked on.
   */
  public breadcrumbClickHandler(crumb: BreadcrumbItem) {
    if (crumb.link) {
      this.router.navigate([crumb.link]);
    }
  }
}
