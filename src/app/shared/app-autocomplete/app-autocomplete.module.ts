import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppAutocompleteComponent } from './app-autocomplete/app-autocomplete.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [
    AppAutocompleteComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatAutocompleteModule
  ],
  exports: [
    AppAutocompleteComponent
  ]
})
export class AppAutocompleteModule { }
