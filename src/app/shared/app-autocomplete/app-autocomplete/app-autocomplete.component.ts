import { Component, Input, OnDestroy } from '@angular/core';
import { AppAutocompleteDelegate } from '../types/app-autocomplete-delegate.model';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './app-autocomplete.component.html',
  styleUrls: ['./app-autocomplete.component.less']
})
export class AppAutocompleteComponent implements OnDestroy {

  /**
   * Placeholder.
   */
  @Input() placeholder: string;

  /**
   * Label.
   */
  @Input() label: string;

  /**
   * Label.
   */
  @Input() hint: string;

  /**
   * Disabled flag.
   */
  @Input() disabled: boolean;

  /**
   * Autocomplete delegate.
   */
  @Input() public autocompleteDelegate = new AppAutocompleteDelegate<any>({
    displayProperties: ['name']
  });

  /**
   * Autocomplete control input.
   */
  @Input() public set autocompleteControl(control: FormControl) {
    this._autocompleteControl = control;
    this.setupListener();
  }

  public get autocompleteControl(): FormControl {
    return this._autocompleteControl;
  }

  private _autocompleteControl = new FormControl('', []);

  private controlSub: Subscription;

  constructor() { }

  /**
   * Setup the listener for value changes so that we can easilty trigger the filter fn
   * on the autocomplete object.
   */
  private setupListener(): void {
    this.autocompleteDelegate.filter(null);
    if (this.autocompleteControl && this.autocompleteControl.valueChanges) {
      // Set up the subscription and include the possible debounce as a pipe.
      this.controlSub = this.autocompleteControl.valueChanges.pipe(
        debounceTime(this.autocompleteDelegate.debounce)
      )
        .subscribe(value => {
          // Actual value sub.
          if (!this.disabled) {
            // As long as not disabled run the filter.
            this.autocompleteDelegate.filter(value);
          }
        });
    }
  }

  ngOnDestroy(): void {
    if (this.controlSub) {
      this.controlSub.unsubscribe();
    }
  }
}
