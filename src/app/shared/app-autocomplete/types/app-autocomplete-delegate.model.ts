import { VariableInstanceLoader } from '../../../common/graphql-helpers/variable-instance-loader';

export class AppAutocompleteDelegate<T> {

  /**
   * Whole list of potential results.
   */
  public list: T[];

  /**
   * Filtered list.
   */
  public filteredList: T[];

  /**
   * Pk property of results.
   */
  public pkProperty = 'id';

  /**
   * What to display.
   */
  public displayProperties: string[];

  /**
   * Filter props.
   */
  public filterProperties: string[];

  /**
   * Is the autocomplete multiple selection or not.
   */
  public multi: boolean;

  /**
   * Selected item or items.
   */
  public selected: T | T[];

  /**
   * Override the filter on list with another call that returns T.
   */
  public filterOverrideHandler: (val: string) => Promise<T[]>;

  /**
   * If there is a debounce for the filter.
   */
  public debounce = 0;

  constructor(opts?: Partial<AppAutocompleteDelegate<T>>) {
    VariableInstanceLoader(this, opts);
  }

  /**
   * Filter function.
   *
   * @param term search term.
   */
  public filter(term: string): void {
      if (!this.filterOverrideHandler) {
        const words = term.split(' ');
        if (this.filterProperties) {
        this.filteredList = this.list.filter(item => (
          this.filterProperties.map(prop => item[prop]).some((propVal) => (
            words.find(word => propVal.includes(word))
          ))
        ));
      } else {
        this.filteredList = this.list;
      }
    } else {
      // Use the external func.
      this.filterOverrideHandler(term)
      .then((items: T[]) => {
        this.list = items;
        this.filteredList = this.list;
      });
    }
  }

  /**
   * Defines how we manage option selections in the autocomplete, we would like
   * to store them in some list or var for ref.
   *
   * @param item new chosen item.
   */
  public selectItem(item: T) {
    if (this.multi) {
      this.selected = ((this.selected || []) as any[]);
      this.selected.push(item);
    } else {
      this.selected = item;
    }
  }

  /**
   * Display funk
   */
  public display = (record: T) => {
    if (!this.displayProperties?.length) {
      throw new Error('AppAutocompleteDelegateError: No display properties were set for object.');
    }
    let displayString: any = record;
    if (record && typeof record === 'object') {
      displayString = this.displayProperties.map(prop => record[prop]).join(' ');
    }
    return displayString;
  }

  /**
   * Map selected elements to primary keys.
   */
  public mapSelectedToPks(): any | any[] {
    if (this.multi) {
      return (this.selected as T[]).map(item => item[this.pkProperty]);
    }
    return this.selected[this.pkProperty];
  }
}
