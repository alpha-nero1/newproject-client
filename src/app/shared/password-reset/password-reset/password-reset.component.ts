/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../auth/auth.service';
import { Utils } from '../../../utils/utils';
import { Validator, ValidResponse } from '../../../common/validator.service';
import { MatSnackBar } from '@angular/material/snack-bar';

/**
 * @author Alessandro Alberga
 * @description password reset component.
 */
@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit, OnDestroy {

  /**
   * Password input.
   */
  public passwordInput = '';

  /**
   * Password confirm input.
   */
  public passwordConfirmInput = '';

  /**
   * Password is valid reference.
   */
  public passwordValidity: ValidResponse = {
    valid: false,
    reasons: []
  };

  /**
   * Param subscription reference.
   */
  private paramSubscription: Subscription;

  private resetToken: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private snack: MatSnackBar,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.paramSubscription = this.activatedRoute.params.subscribe(this.activeRouteHandler);
  }

  /**
   * Param subscription handler.
   */
  private activeRouteHandler = params => {
    this.resetToken = params.reset_token;
  }

  /**
   * Evaluate the passwords on type.
   */
  public onPasswordInputChange(): void {
    const valid: ValidResponse = { reasons: [] };
    const passwordValid = Validator.validatePassword(this.passwordInput, this.passwordConfirmInput, false);
    const passwordConfirmValid = Validator.validatePassword(this.passwordConfirmInput, this.passwordInput, true);
    valid.valid = (passwordValid.valid && passwordConfirmValid.valid);
    valid.reasons = Utils.GetUnique([...passwordValid.reasons, ...passwordConfirmValid.reasons]);
    this.passwordValidity = valid;
  }

  /**
   * On password reset.
   */
  public onResetPassword(): void {
    if (this.passwordValidity.valid) {
      this.authService.finalisePasswordReset(this.resetToken, this.passwordInput)
        .then(res => {
          if (res) {
            this.router.navigate(['/auth/login/']);
            this.snack.open('Password reset succesfully!');
          } else {
            this.snack.open('Password reset failed, please try again.');
          }
        });
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
