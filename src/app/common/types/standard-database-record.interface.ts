/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description Defines a base interface that database like interfaces can/should extend.
 */
export interface StandardDatabaseRecord {

  /**
   * Id of the reocrd.
   */
  id?: number;

  /**
   * Disabled state of record.
   */
  disabled?: boolean;

  /**
   * Updated at date of record.
   */
  updated_at?: any;

  /**
   * Created at date of record.
   */
  created_at?: any;
}
