/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { User } from '../auth/user.model';

/**
 * @author Alessandro Alberga
 * @description interfcae defining the format we get subscriptions in from the backend.
 */
export interface SubscriptionResponse {

  /**
   * Count of the rows.
   */
  count: number;

  /**
   * Rows in the response.
   */
  rows: Sub[];
}


/**
 * @description Sub interface, needs reference only in this file.
 */
interface Sub {
  /**
   * Id of the subscription.
   */
  id: number;

  /**
   * User id of user who subscribed.
   */
  user_id: number;

  /**
   * User id of the target user of the subscription.
   */
  subscribee_id: number;

  /**
   * Created at timestamp of the subscription.
   */
  created_at: Date;

  /**
   * Subscribing user.
   */
  subscriber?: User;

  /**
   * User subscribed to.
   */
  subscribee?: User;
}
