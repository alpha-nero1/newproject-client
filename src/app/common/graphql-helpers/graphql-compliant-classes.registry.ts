/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description Object mapping typenames to classes that are instantiatable for that typename.
 */
export const GraphqlCompliantClasses = {
  // tslint:disable-next-line: object-literal-key-quotes
  'Site': import('./../../site/site.model').then(res => res.Site)
};
