/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { GraphqlCompliantClasses } from './graphql-compliant-classes.registry';

/**
 * Take an instance of this and use object keys to assign vars to it.
 */
export const VariableInstanceLoader = (instance, opts) => {
  if (instance && opts) {
    Object.keys(opts).forEach(key => {
      instance[key] = opts[key];
    });
  }
};
