/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';
import { User } from '../auth/user.model';

/**
 * @author Alessandro Alberga
 * @description a component extends this class if it wishes to be aware of the
 *   active user's data.
 */
export class UserAware implements OnInit, OnDestroy {

  /**
   * User reference.
   */
  private _user: User;

  /**
   * Subscription to the user.
   */
  public userSubscription: Subscription;

  /**
   * Callback to execute on user change.
   */
  public userChangedCallback: (user) => void;

  constructor(
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    this.userSubscription = this.authService.userObservable
    .subscribe((user: User) => {
      this._user = user;
    });
  }

  /**
   * Initialise the callback to fire on user change. Would be in constructor if
   * there wasn't `this` keyword restriction for extending classes.
   *
   * @param userChangedCallback callback to fire on user change.
   */
  public initCallback(userChangedCallback = (user) => null): void {
    this.userChangedCallback = userChangedCallback;
    this.userSubscription = this.authService.userObservable
    .subscribe((user: User) => {
      this._user = user;
      this.userChangedCallback(user);
    });
  }

  get user() {
    return this._user;
  }

  /**
   * Leverage the user service to fetch a promise of the current
   * user instance.
   */
  public thenableUser(): Promise<User> {
    return this.authService.getUser();
  }

  ngOnDestroy(): void {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }
}
