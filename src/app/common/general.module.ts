/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

/**
 * @author Alessandro Alberga
 * @description bundles common module patterns for easy use accross the app.
 */
@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    FormsModule
  ],
  exports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    FormsModule
  ]
})
export class GeneralModule {}
