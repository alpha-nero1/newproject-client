/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import {
  Directive,
  ElementRef,
  Output,
  EventEmitter,
  OnDestroy,
  Input
} from '@angular/core';

/**
 * @author Alessandro Alberga
 * @description Directive for emitting when element has become seen.
 */
@Directive({
  selector: '[appDetectInView]',
})
export class DetectInViewDirective implements OnDestroy {

  /**
   * Input for already seen.
   */
  @Input() set seen(seen: boolean) {
    if (seen) {
      window.removeEventListener('scroll', this.hasScrolledIntoView, true);
    } else {
      window.addEventListener('scroll', this.hasScrolledIntoView, true);
    }
    // Run an initial check in case scrolling cant happen
    // but we want something to be checked.
    this.hasScrolledIntoView();
  }

  /**
   * On scroll it will emit if the element is in view or not.
   */
  @Output() isInView = new EventEmitter<boolean>();

  constructor(private el: ElementRef) { }

  /**
   * Has scrolled into view check.
   */
  public hasScrolledIntoView = () => {
    const { top: elementTop, bottom: elementBottom } = this.el.nativeElement.getBoundingClientRect();
    const scrolled = Boolean((elementTop >= 0) && (elementBottom <= window.innerHeight));
    this.isInView.emit(scrolled);
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.hasScrolledIntoView, true);
  }
}
