/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetectInViewDirective } from './detect-in-view.directive';

/**
 * @author Alessandro Alberga
 * @description Detect in view wrapper module.
 */
@NgModule({
  declarations: [DetectInViewDirective],
  imports: [CommonModule],
  exports: [DetectInViewDirective],
})
export class DetectInViewModule { }
