/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import validator from 'validator';

/**
 * @author Alessandro Alberga
 * @description class containing static validation functions.
 */
export class Validator {

  /**
   * Vaidate a password.
   *
   * @param password password text.
   * @param passwordConfirm optional password to match with first.
   */
  public static validatePassword(password: string, passwordConfirm?: string, typingCompare = false): ValidResponse {
    let valid: ValidResponse = { text: password, valid: true, reasons: [], type: 'password' };
    const specialCharacterFormat = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    const numberFormat = /[1234567890]+/;
    if (password && !typingCompare) {
      if (password.length < 6) {
        valid = { ...valid, valid: false, reasons: [...valid.reasons, 'Password must be atleast 6 characters long.'] };
      }
      if (!specialCharacterFormat.test(password)) {
        valid = { ...valid, valid: false, reasons: [...valid.reasons, 'Password must contain at least one special character.'] };
      }
      if (!numberFormat.test(password)) {
        valid = { ...valid, valid: false, reasons: [...valid.reasons, 'Password must contain at least one number.'] };
      }
    } else if (!password && !typingCompare) {
      valid.valid = false;
    } else {
      if (passwordConfirm && passwordConfirm !== password) {
        valid = { ...valid, valid: false, reasons: [...valid.reasons, 'Passwords are not the same.'] };
      }
    }
    return valid;
  }

  /**
   * Validate an email.
   *
   * @param email email to validate.
   */
  public static validateEmail(email: string): ValidResponse {
    let valid: ValidResponse = { text: email, valid: true, reasons: [], type: 'email' };
    if (!validator.isEmail(email)) {
      valid = { ...valid, valid: false, reasons: ['Invalid email'] };
    }
    return valid;
  }

  /**
   * Validate a user handle.
   *
   * @param handle handle text to validate.
   */
  public static validateHandle(handle: string): ValidResponse {
    let valid: ValidResponse = { text: handle, valid: true, reasons: [], type: 'handle' };
    if (handle && handle.length < 6) {
      valid = { ...valid, valid: false, reasons: ['Handle must be at least 6 characters long.']};
    }
    return valid;
  }
}

export interface ValidResponse {
  text?: string;
  valid?: boolean;
  reasons?: string[];
  type?: string;
}
