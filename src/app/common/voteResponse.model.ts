/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description interface description of a vote response (how votes are sent from API)
 */
export interface VoteResponse {

  /**
   * Count of upvotes.
   */
  upvote_count?: number;

  /**
   * List of users who have upvoted (is a limited list).
   */
  upvotes?: any[];

  /**
   * Count of downvotes.
   */
  downvote_count?: number;

  /**
   * List of users who have downvoted (is a limited list).
   */
  downvotes?: any[];
}
