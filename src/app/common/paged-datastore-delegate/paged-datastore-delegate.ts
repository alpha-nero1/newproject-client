/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { VariableInstanceLoader } from '../graphql-helpers/variable-instance-loader';
import { GeneralQueryOpts, SortValue } from '../../shared/types/filter.interface';
import { NPTableObject } from '../../shared/np-table/types/np-table-object.model';

/**
 * @author Alessandro Alberga.
 * @description A delegate object managing pages and storing of data.
 */
export class PagedDatastoreDelegate<T> {
  /**
   * Paged data store.
   */
  public data: PagedDatastoreEntry<T>[] = [];

  /**
   * The list of data.
   */
  public dataList: T[] = [];

  /**
   * Query opts for the fetching.
   */
  public queryOpts: GeneralQueryOpts = {
    page: 0,
    page_size: 100,
    sort_by_property: 'created_at',
    sort_by_value: SortValue.Descending
  };

  /**
   * Table obejct we are leveraging for paging data.
   */
  public datatableObj = new NPTableObject<T>();

  /**
   * Allow a special function for created at.
   */
  public createdAtFormatterFunction: (date) => void;

  /**
   * What is to be called when a new page is called for.
   */
  public loadResultsCallback: (opts: GeneralQueryOpts) => Promise<{ count: number, rows: T[] }>;

  /**
   * What needs to be executed to add an item to the set.
   */
  public addItemCallback: (item: T) => Promise<any>;

  /**
   * What is to be called when an item needs to be removed from the set.
   */
  public removeItemCallback: (itemId) => Promise<any>;

  /**
   * Specify a callback to refresh results.
   */
  public refreshResultsCallback: (idsToRefresh: number[], opts: GeneralQueryOpts) => Promise<T[]>;

  /**
   * Attributes to preserve if null on refresh.
   */
  public preserveAttributes: string[];

  constructor(opts?: Partial<PagedDatastoreDelegate<T>>) {
    VariableInstanceLoader(this, opts);
  }

  /**
   * Takes an items id and refreshes its page.
   *
   * @param itemId refresh page of item id.
   * @param page page of item.
   */
  public refreshPageOfItem(itemId: number, page?: number): Promise<any> {
    const pageToUpsert = this.getPageOfItem(itemId);
    // If we sent in a page override use its ids instead.
    const pageIds = page ? this.getElementIdsInPage(page) : this.getAllPagesIdsFromElementId(itemId);
    if (!this.refreshResultsCallback) {
      throw new Error('**ERROR: You failed to specify refreshResultsCallback');
    }
    // Execute callback.
    return this.refreshResultsCallback(pageIds, this.queryOpts)
    .then((newRows) => {
      this.upsertDataStore(
        page || pageToUpsert,
        newRows
      );
    });
  }

  /**
   * Add an item to the datastore.
   *
   * @param item new item to add.
   */
  public addItemToDatastore(item: T): Promise<any> {
    if (!this.addItemCallback) {
      throw new Error('**ERROR: You failed to specify addItemCallback');
    }
    return this.addItemCallback(item)
      .then(() => {
        if (this.queryOpts.sort_by_value === SortValue.Descending) {
          this.shiftLastElementDown(0);
          this.loadResults(0);
        } else {
          const samePage = (
            (this.findPage(this.queryOpts.page).rows.length >= this.queryOpts.page_size) ?
            null :
            true
          );
          this.loadResults(null, samePage);
        }
      });
  }

  /**
   * Remove an item from the datastore.
   *
   * @param itemId id of the item to remove.
   */
  public removeItemFromDatastore(itemId: number): Promise<any> {
    if (!this.removeItemCallback) {
      throw new Error('**ERROR: You failed to specify removeItemCallback');
    }
    return this.removeItemCallback(itemId)
    .then(() => {
      const pageOfRecord = this.getPageOfItem(itemId);
      if (this.queryOpts.sort_by_value === SortValue.Descending) {
        this.shiftFirstElementUp(pageOfRecord);
        this.loadResults(pageOfRecord);
      } else {
        this.loadResults(pageOfRecord);
      }
    });
  }

  /**
   * Get the page of a particular item from its id.
   *
   * @param id id to search for page.
   */
  public getPageOfItem(id: number): number {
    const rowsArr = this.data.map(entry => entry.rows);
    const pageIndex = rowsArr.findIndex(innerRowsArr => (
      Boolean(innerRowsArr.find((item: any) => item.id === id))
    ));
    return this.data[pageIndex]?.page;
  }

  /**
   * For insertions on a descending result set.
   *
   * @param page page to shift all elements down afterwards.
   */
  private shiftLastElementDown(page: number): void {
    // Find this page.
    const thisPage = this.findPage(page);
    if (thisPage) {
      const lastRowOfPageIndex = thisPage.rows.length - 1;
      const rowsToKeepOnThisPage = thisPage.rows.filter((row, index) => index !== lastRowOfPageIndex);
      const lastOfThisPage = thisPage.rows[lastRowOfPageIndex];
      if (lastRowOfPageIndex === this.queryOpts.page_size - 1) {
        const nextPage = page + 1;
        const nextPageEntry = this.findPage(nextPage);

        // If the next page entry exists.
        const rowsThatNextPageWillHave = (
          nextPageEntry ?
          [lastOfThisPage, ...nextPageEntry.rows] :
          [lastOfThisPage]
        );
        this.upsertDataStore(nextPage, rowsThatNextPageWillHave);
        this.upsertDataStore(page, rowsToKeepOnThisPage);
        if (nextPageEntry) {
          this.shiftLastElementDown(nextPage);
        }
      }
    }
  }

  /**
   * Shoft first element up from page.
   *
   * @page page to shift afterwards.
   */
  private shiftFirstElementUp(page: number): void {
    const thisPage = this.findPage(page);
    if (thisPage) {
      const nextPage = page + 1;
      const nextPageEntry = this.findPage(nextPage);
      if (nextPageEntry) {
        // If the next page entry exists.
        const rowsThatThisPageWillHave = (
          (nextPageEntry && nextPageEntry?.rows[0]) ?
          [...thisPage.rows, nextPageEntry.rows[0]] :
          thisPage.rows
        );
        const rowsToKeepOnNextPage = nextPageEntry.rows.splice(0, 1);
        this.upsertDataStore(page, rowsThatThisPageWillHave);
        this.upsertDataStore(nextPage, rowsToKeepOnNextPage);
        this.shiftFirstElementUp(nextPage);
      }
    }
  }

  /**
   * Get all ids in page from an element id.
   *
   * @param id id of element.
   */
  public getAllPagesIdsFromElementId(id: number): number[] {
    return this.getElementIdsInPage(
      this.getPageOfItem(id)
    );
  }

  /**
   * Gets the ids of elements in a particular page.
   */
  public getElementIdsInPage(page: number): number[] {
    const dataSubset = this.findPage(page)?.rows;
    return dataSubset?.map((element: any) => element.id);
  }

  /**
   * Find the page by page number.
   */
  private findPage(page: number): PagedDatastoreEntry<T> {
    return this.data.find(pageSet => pageSet.page === page);
  }

  /**
   * Load more reuslts from the API, allows for specification of a specific page.
   *
   * @param specificPage optional page to pass in.
   * @param we can specify if we want to load the current page again.
   */
  public loadResults(specificPage?: number, currentPage?: boolean): Promise<void> {
    if (!this.loadResultsCallback) {
      throw new Error('**ERROR: You tried to load results without setting the loadResultsCallback');
    }
    let queryOptsToUse = {
      ...this.queryOpts
    };
    // If we sent in a specific page we want to reload that one.
    // Otherwise we use the ongoing page.
    if (specificPage === 0 || specificPage > 0) {
      queryOptsToUse.page = specificPage;
    } else if (!currentPage) {
      this.queryOpts.page = (this.queryOpts.page + 1);
      queryOptsToUse = {
        ...this.queryOpts
      };
    }
    this.datatableObj.loading = true;
    return this.loadResultsCallback(queryOptsToUse)
    .then(({ count, rows }) => {
      this.upsertDataStore(queryOptsToUse.page, rows, count);
      this.datatableObj.loading = false;
    })
    .catch(err => {
      this.datatableObj.loading = false;
      throw err;
    });
  }

  /**
   * Get the whole unified list.
   */
  public updateListFromStore(): void {
    let dataListToSet = [];
    this.data.forEach(entry => {
      dataListToSet = [...dataListToSet, ...entry.rows];
    });
    this.dataList = dataListToSet;
  }

  /**
   * Upsert on the local store.
   *
   * @param page page number
   * @param newRows new rows to insert.
   */
  public upsertDataStore(page: number, newRows: any[], totalResults = this.datatableObj.count) {
    if (!newRows) { return; }
    const foundIndex = this.data.findIndex(pageSet => pageSet.page === page);
    if (foundIndex > -1) {
      // Found the entry lets set refreshed.
      const oldRow = (row) => this.data[foundIndex].rows.find((trow: any) => trow.id === row.id);
      this.data[foundIndex].rows = newRows.map((row, i) => this.processRow(row, oldRow(row)));
    } else {
      // We dont have this set, add it!
      this.data.push({
        page,
        rows: newRows.map(row => ({
          ...row,
          created_at: (
            this.createdAtFormatterFunction ?
            this.createdAtFormatterFunction(row.created_at) :
            row.created_at
          )
        }))
      });
    }
    // After all is said and done, update the unified list.
    this.updateListFromStore();
    this.datatableObj.setData(this.dataList);
    this.datatableObj.setCount(totalResults);
  }

  /**
   * Process a row to keep preserved attributes.
   *
   * @param newRow new row.
   * @param oldRow equivalent old row.
   */
  private processRow(newRow: T, oldRow: T): T {
    const attributes = this.preserveAttributes ? this.preserveAttributes : [];
    const keepObj = { };
    attributes.forEach(key => {
      if (newRow && !newRow[key]) {
        keepObj[key] = oldRow[key];
      }
    });
    return {
      ...newRow,
      ...keepObj
    };
  }

  /**
   * Common check if there is more data to load.
   */
  public thereIsMoreDataToLoad(): boolean {
    return (
      this.dataList?.length <
      (this.datatableObj?.count)
    );
  }

  /**
   * Run a reset of the object.
   */
  public reset(): void {
    this.queryOpts = {
      ...this.queryOpts,
      page: 0
    };
    this.dataList = [];
    this.data = [];
  }
}

/**
 * Entries that make up the paged datastore!
 */
export interface PagedDatastoreEntry<T> {
  page: number;
  rows: T[];
}
