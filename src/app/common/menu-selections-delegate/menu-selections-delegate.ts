/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { VariableInstanceLoader } from '../graphql-helpers/variable-instance-loader';

/**
 * @author Alessandro Alberga
 * @description Class to facilitate menus ans selecting.
 */
export class MenuSelectionsDelegate {

  /**
   * Menu selections.
   */
  public menuSelections: MenuSelection[] = [];

  /**
   * The active menu selection.
   */
  public activeMenuSelection: MenuSelection;

  constructor(opts: Partial<MenuSelectionsDelegate>) {
    VariableInstanceLoader(this, opts);
  }

  /**
   * Select a menu item and set as active.
   *
   * @param pos posiiton of menu to select.
   */
  public selectMenu(pos: number): void {
    this.activeMenuSelection = this.getMenuItemByPosition(pos);
  }

  /**
   * Get menu item from possible with pos value.
   *
   * @param pos position of element to fetch.
   */
  public getMenuItemByPosition(pos: number): MenuSelection {
    return this.menuSelections.find(item => item.position === pos);
  }
}

export interface MenuSelection {

  /**
   * Name of selection.
   */
  name?: string;

  /**
   * Position of menu.
   */
  position?: number;

  /**
   * Icon of selection.
   */
  icon?: string;
}
