/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisplayNumberPipe } from './display-number.pipe';

/**
 * @author Alessandro Alberga
 * @description Display number pipe.
 */
@NgModule({
  declarations: [ DisplayNumberPipe ],
  imports: [ CommonModule ],
  exports: [ DisplayNumberPipe ],
})
export class DisplayNumberPipeModule { }
