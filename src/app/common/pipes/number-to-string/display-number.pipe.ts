/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Pipe, PipeTransform } from '@angular/core';

/**
 * @author Alessandro Alberga
 * @description Nicely displays numbersfor us. Special thanks to:
 * mQuiroz here https://stackoverflow.com/questions/48583020/display-number-in-million-or-thousand-format-in-angular-4
 */
@Pipe({
  name: 'displayNumber'
})
export class DisplayNumberPipe implements PipeTransform {
  public transform(input: any, args?: any): any {
    let exponent;
    const numberSuffixes = ['k', 'M', 'G', 'T', 'P', 'E'];
    if (Number.isNaN(input)) {
      return null;
    }
    // Just return what we have, it is sufficient.
    if (input < 1000) {
      return input;
    }
    exponent = Math.floor(Math.log(input) / Math.log(1000));
    return (input / Math.pow(1000, exponent)).toFixed(args) + numberSuffixes[exponent - 1];
  }
}
