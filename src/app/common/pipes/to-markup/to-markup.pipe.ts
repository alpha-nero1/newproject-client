/**
 * This source code is the confidential, proprietary information of
 * New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with New Project Pty Ltd.
 *
 * 2020: New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Pipe, PipeTransform } from '@angular/core';
import * as marked from 'marked';

/**
 * @author Alessandro Alberga
 * @description Takes markdown input and returns mark up input.
 */
@Pipe({
  name: 'toMarkup'
})
export class ToMarkupPipe implements PipeTransform {
  public transform(value: any, ...args: any[]): any {
    if (value && value.length) {
      return marked(value);
    }
    return value;
  }
}
