import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToMarkupPipe } from './to-markup.pipe';

@NgModule({
  declarations: [
    ToMarkupPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ToMarkupPipe
  ]
})
export class ToMarkupModule { }
