/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';
import { TokenInterceptor } from './utils/token.inteceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';
import { AuthGuardService } from './auth/auth.guard.service';
import { GraphQLService } from './utils/graphQL.service';
import { Apollo } from 'apollo-angular';
import { createApollo } from './apollo-link';
import { Utils } from './utils/utils';
import { AwsS3Service } from './third-party/aws.s3.service';
import { NavbarModule } from './navbar/navbar.module';
import { AuthService } from './auth/auth.service';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressBarModule } from '@angular/material/progress-bar';

/**
 * @author Alessandro Alberga
 * @description Main app module.
 */
@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    HttpLinkModule,
    FormsModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatChipsModule,
    NavbarModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatMenuModule,
    FlexLayoutModule,
    MatSnackBarModule,
    MatProgressBarModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink]
    },
    AuthGuardService,
    // For our app infrastructure to work this can be the only provider instance
    // of auth service.
    AuthService,
    GraphQLService,
    Apollo,
    Utils,
    AwsS3Service
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
